declare module "@salesforce/apex/customLookUpController.fetchLookUpValues" {
  export default function fetchLookUpValues(param: {searchKeyWord: any, ObjectName: any}): Promise<any>;
}
declare module "@salesforce/apex/customLookUpController.fetchUserQueueValues" {
  export default function fetchUserQueueValues(param: {searchKeyWord: any}): Promise<any>;
}
declare module "@salesforce/apex/customLookUpController.getCSATValues" {
  export default function getCSATValues(param: {projectId: any}): Promise<any>;
}
declare module "@salesforce/apex/customLookUpController.getCalculatedData" {
  export default function getCalculatedData(): Promise<any>;
}
