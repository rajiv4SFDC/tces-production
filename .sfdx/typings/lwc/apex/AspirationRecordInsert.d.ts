declare module "@salesforce/apex/AspirationRecordInsert.createBehaviourRecord" {
  export default function createBehaviourRecord(param: {aspirationSkillList: any, duration: any}): Promise<any>;
}
declare module "@salesforce/apex/AspirationRecordInsert.createIndustryRecord" {
  export default function createIndustryRecord(param: {aspirationIndustryList: any, duration: any}): Promise<any>;
}
declare module "@salesforce/apex/AspirationRecordInsert.createSFProductRecord" {
  export default function createSFProductRecord(param: {aspirationSFProductList: any, duration: any}): Promise<any>;
}
declare module "@salesforce/apex/AspirationRecordInsert.createBusinessProcessRecord" {
  export default function createBusinessProcessRecord(param: {aspirationBusinessProcessList: any, duration: any}): Promise<any>;
}
declare module "@salesforce/apex/AspirationRecordInsert.createCoreSkillRecord" {
  export default function createCoreSkillRecord(param: {aspirationCoreSkillList: any, duration: any}): Promise<any>;
}
declare module "@salesforce/apex/AspirationRecordInsert.createAspPersonalRecord" {
  export default function createAspPersonalRecord(param: {aspirationPersonalList: any, duration: any}): Promise<any>;
}
declare module "@salesforce/apex/AspirationRecordInsert.createAspProfessionalRecord" {
  export default function createAspProfessionalRecord(param: {aspirationProfessionalList: any, duration: any}): Promise<any>;
}
declare module "@salesforce/apex/AspirationRecordInsert.createTHRecord" {
  export default function createTHRecord(param: {aspirationTHList: any, duration: any}): Promise<any>;
}
declare module "@salesforce/apex/AspirationRecordInsert.createCertificateRecord" {
  export default function createCertificateRecord(param: {aspirationCertRecordList: any, duration: any}): Promise<any>;
}
declare module "@salesforce/apex/AspirationRecordInsert.createCerApplyRecord" {
  export default function createCerApplyRecord(param: {AspirationCertApplyList: any, duration: any}): Promise<any>;
}
declare module "@salesforce/apex/AspirationRecordInsert.getPicklistValues" {
  export default function getPicklistValues(): Promise<any>;
}
declare module "@salesforce/apex/AspirationRecordInsert.getPicklistValuesIndustry" {
  export default function getPicklistValuesIndustry(): Promise<any>;
}
declare module "@salesforce/apex/AspirationRecordInsert.getPicklistValuesSFProduct" {
  export default function getPicklistValuesSFProduct(): Promise<any>;
}
declare module "@salesforce/apex/AspirationRecordInsert.getPicklistValuesBusinessProcess" {
  export default function getPicklistValuesBusinessProcess(): Promise<any>;
}
declare module "@salesforce/apex/AspirationRecordInsert.getPicklistValuesCoreSkill" {
  export default function getPicklistValuesCoreSkill(): Promise<any>;
}
declare module "@salesforce/apex/AspirationRecordInsert.getPicklistValuesTH" {
  export default function getPicklistValuesTH(): Promise<any>;
}
declare module "@salesforce/apex/AspirationRecordInsert.getPicklistValuesCertificates" {
  export default function getPicklistValuesCertificates(): Promise<any>;
}
declare module "@salesforce/apex/AspirationRecordInsert.getPicklistValuesApprove" {
  export default function getPicklistValuesApprove(): Promise<any>;
}
declare module "@salesforce/apex/AspirationRecordInsert.getPicklistValuesCredentials" {
  export default function getPicklistValuesCredentials(): Promise<any>;
}
declare module "@salesforce/apex/AspirationRecordInsert.getPicklistValuesCertifications" {
  export default function getPicklistValuesCertifications(): Promise<any>;
}
declare module "@salesforce/apex/AspirationRecordInsert.getPicklistStatusValuesTH" {
  export default function getPicklistStatusValuesTH(): Promise<any>;
}
declare module "@salesforce/apex/AspirationRecordInsert.getPicklistValuesCertificationStatus" {
  export default function getPicklistValuesCertificationStatus(): Promise<any>;
}
declare module "@salesforce/apex/AspirationRecordInsert.getDisplayDuration" {
  export default function getDisplayDuration(): Promise<any>;
}
