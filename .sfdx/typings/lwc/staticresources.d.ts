declare module "@salesforce/resourceUrl/AddIcon" {
    var AddIcon: string;
    export default AddIcon;
}
declare module "@salesforce/resourceUrl/AddRow" {
    var AddRow: string;
    export default AddRow;
}
declare module "@salesforce/resourceUrl/AssetSavebutton" {
    var AssetSavebutton: string;
    export default AssetSavebutton;
}
declare module "@salesforce/resourceUrl/BackLogo" {
    var BackLogo: string;
    export default BackLogo;
}
declare module "@salesforce/resourceUrl/BootStrap" {
    var BootStrap: string;
    export default BootStrap;
}
declare module "@salesforce/resourceUrl/CRpdfcss" {
    var CRpdfcss: string;
    export default CRpdfcss;
}
declare module "@salesforce/resourceUrl/ChartJS23" {
    var ChartJS23: string;
    export default ChartJS23;
}
declare module "@salesforce/resourceUrl/CompletedNew" {
    var CompletedNew: string;
    export default CompletedNew;
}
declare module "@salesforce/resourceUrl/Correct" {
    var Correct: string;
    export default Correct;
}
declare module "@salesforce/resourceUrl/DeferredNew" {
    var DeferredNew: string;
    export default DeferredNew;
}
declare module "@salesforce/resourceUrl/DeleteRow" {
    var DeleteRow: string;
    export default DeleteRow;
}
declare module "@salesforce/resourceUrl/FeedBackIcon" {
    var FeedBackIcon: string;
    export default FeedBackIcon;
}
declare module "@salesforce/resourceUrl/GoalEditIcon" {
    var GoalEditIcon: string;
    export default GoalEditIcon;
}
declare module "@salesforce/resourceUrl/Grey" {
    var Grey: string;
    export default Grey;
}
declare module "@salesforce/resourceUrl/InProgressNew" {
    var InProgressNew: string;
    export default InProgressNew;
}
declare module "@salesforce/resourceUrl/JqueryA" {
    var JqueryA: string;
    export default JqueryA;
}
declare module "@salesforce/resourceUrl/KVPAppsLogo" {
    var KVPAppsLogo: string;
    export default KVPAppsLogo;
}
declare module "@salesforce/resourceUrl/KVPLogo" {
    var KVPLogo: string;
    export default KVPLogo;
}
declare module "@salesforce/resourceUrl/KVPLogo1" {
    var KVPLogo1: string;
    export default KVPLogo1;
}
declare module "@salesforce/resourceUrl/KVP_Pdf_New" {
    var KVP_Pdf_New: string;
    export default KVP_Pdf_New;
}
declare module "@salesforce/resourceUrl/MOM_Styles" {
    var MOM_Styles: string;
    export default MOM_Styles;
}
declare module "@salesforce/resourceUrl/MarkAttendanceLogo" {
    var MarkAttendanceLogo: string;
    export default MarkAttendanceLogo;
}
declare module "@salesforce/resourceUrl/OlympicDataService" {
    var OlympicDataService: string;
    export default OlympicDataService;
}
declare module "@salesforce/resourceUrl/Orange" {
    var Orange: string;
    export default Orange;
}
declare module "@salesforce/resourceUrl/PageStyle1" {
    var PageStyle1: string;
    export default PageStyle1;
}
declare module "@salesforce/resourceUrl/Plus" {
    var Plus: string;
    export default Plus;
}
declare module "@salesforce/resourceUrl/Query" {
    var Query: string;
    export default Query;
}
declare module "@salesforce/resourceUrl/Red" {
    var Red: string;
    export default Red;
}
declare module "@salesforce/resourceUrl/Required" {
    var Required: string;
    export default Required;
}
declare module "@salesforce/resourceUrl/Signature" {
    var Signature: string;
    export default Signature;
}
declare module "@salesforce/resourceUrl/SiteCSS" {
    var SiteCSS: string;
    export default SiteCSS;
}
declare module "@salesforce/resourceUrl/SiteSamples" {
    var SiteSamples: string;
    export default SiteSamples;
}
declare module "@salesforce/resourceUrl/Smiley1" {
    var Smiley1: string;
    export default Smiley1;
}
declare module "@salesforce/resourceUrl/Smiley2" {
    var Smiley2: string;
    export default Smiley2;
}
declare module "@salesforce/resourceUrl/Smiley3" {
    var Smiley3: string;
    export default Smiley3;
}
declare module "@salesforce/resourceUrl/Smiley4" {
    var Smiley4: string;
    export default Smiley4;
}
declare module "@salesforce/resourceUrl/Smiley5" {
    var Smiley5: string;
    export default Smiley5;
}
declare module "@salesforce/resourceUrl/TableHeader" {
    var TableHeader: string;
    export default TableHeader;
}
declare module "@salesforce/resourceUrl/ThumbDown" {
    var ThumbDown: string;
    export default ThumbDown;
}
declare module "@salesforce/resourceUrl/ThumbUp" {
    var ThumbUp: string;
    export default ThumbUp;
}
declare module "@salesforce/resourceUrl/UtilJS" {
    var UtilJS: string;
    export default UtilJS;
}
declare module "@salesforce/resourceUrl/Wrong" {
    var Wrong: string;
    export default Wrong;
}
declare module "@salesforce/resourceUrl/Yellow" {
    var Yellow: string;
    export default Yellow;
}
declare module "@salesforce/resourceUrl/backimg" {
    var backimg: string;
    export default backimg;
}
declare module "@salesforce/resourceUrl/bootmetro" {
    var bootmetro: string;
    export default bootmetro;
}
declare module "@salesforce/resourceUrl/calShadowLeft" {
    var calShadowLeft: string;
    export default calShadowLeft;
}
declare module "@salesforce/resourceUrl/calShadowRight" {
    var calShadowRight: string;
    export default calShadowRight;
}
declare module "@salesforce/resourceUrl/calnderview" {
    var calnderview: string;
    export default calnderview;
}
declare module "@salesforce/resourceUrl/envolope_image" {
    var envolope_image: string;
    export default envolope_image;
}
declare module "@salesforce/resourceUrl/goalCloseBtn" {
    var goalCloseBtn: string;
    export default goalCloseBtn;
}
declare module "@salesforce/resourceUrl/goalPopupHead" {
    var goalPopupHead: string;
    export default goalPopupHead;
}
declare module "@salesforce/resourceUrl/goalTitleHead" {
    var goalTitleHead: string;
    export default goalTitleHead;
}
declare module "@salesforce/resourceUrl/jQueryNew" {
    var jQueryNew: string;
    export default jQueryNew;
}
declare module "@salesforce/resourceUrl/jQueryTreeView" {
    var jQueryTreeView: string;
    export default jQueryTreeView;
}
declare module "@salesforce/resourceUrl/jquery" {
    var jquery: string;
    export default jquery;
}
declare module "@salesforce/resourceUrl/jquery_ui" {
    var jquery_ui: string;
    export default jquery_ui;
}
declare module "@salesforce/resourceUrl/kvp" {
    var kvp: string;
    export default kvp;
}
declare module "@salesforce/resourceUrl/kvp_shared_idea" {
    var kvp_shared_idea: string;
    export default kvp_shared_idea;
}
declare module "@salesforce/resourceUrl/minus" {
    var minus: string;
    export default minus;
}
declare module "@salesforce/resourceUrl/monitor_image" {
    var monitor_image: string;
    export default monitor_image;
}
declare module "@salesforce/resourceUrl/msgimg" {
    var msgimg: string;
    export default msgimg;
}
declare module "@salesforce/resourceUrl/phone" {
    var phone: string;
    export default phone;
}
declare module "@salesforce/resourceUrl/pic" {
    var pic: string;
    export default pic;
}
declare module "@salesforce/resourceUrl/printIcon" {
    var printIcon: string;
    export default printIcon;
}
declare module "@salesforce/resourceUrl/wordcss" {
    var wordcss: string;
    export default wordcss;
}
