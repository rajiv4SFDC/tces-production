declare module "@salesforce/label/c.CcEmail" {
    var CcEmail: string;
    export default CcEmail;
}
declare module "@salesforce/label/c.Deepti_Email_Id" {
    var Deepti_Email_Id: string;
    export default Deepti_Email_Id;
}
declare module "@salesforce/label/c.Event_Record_ID" {
    var Event_Record_ID: string;
    export default Event_Record_ID;
}
declare module "@salesforce/label/c.ForH1Duration" {
    var ForH1Duration: string;
    export default ForH1Duration;
}
declare module "@salesforce/label/c.ForH2Duration" {
    var ForH2Duration: string;
    export default ForH2Duration;
}
declare module "@salesforce/label/c.ForH3Duration" {
    var ForH3Duration: string;
    export default ForH3Duration;
}
declare module "@salesforce/label/c.Hello" {
    var Hello: string;
    export default Hello;
}
declare module "@salesforce/label/c.MitigationRecorded" {
    var MitigationRecorded: string;
    export default MitigationRecorded;
}
declare module "@salesforce/label/c.SupportRequest_ManageTasks" {
    var SupportRequest_ManageTasks: string;
    export default SupportRequest_ManageTasks;
}
