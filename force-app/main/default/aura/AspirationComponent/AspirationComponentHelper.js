/******************************************
 * Created By  : Ajeet Singh Shekhawat
 * Created On  : 9 Aug 2018
 * Modified By : Ajeet Singh Shekhawat
 * Modified On : 9 Aug 2018
 * Description : Create a Lightning component to capture our team members competency and aspiration. We need a table structure with clearly
 				 identifying areas of strengths and areas of improvements.    
				
******************************************/

({
	BusinessProcessHelper : function(component,event,helper) {
        
        var updatedListSize;
		var learningPriorityArray = [];
		var aspirationBusinessProcessList = component.get("v.AspirationBusinessProcessList");
        var duration = component.get("v.duration");
        var listSizeBusinessProcess = aspirationBusinessProcessList.length;	// calculate length of list coming from component.
        var updatedListSizeBusinessProcess;
        let aspirationBusinessProcessUpdatedList = [];
        var learningPriorityArrayBusinessProcess = [];
        
		         
        var recordTypeMapBusinessProcess = {};		// decalre map to store skills of Behaviour RecordType
        var BusinessProcessList = [];
        BusinessProcessList = component.get("v.optionsBusinessProcess");	// get picklist values from component
        var BusinessProcessListSize = BusinessProcessList.length;
       	console.log('aspirationBusinessProcessList ' + JSON.stringify(aspirationBusinessProcessList));
        for(var i = 0; i < BusinessProcessListSize; i++) {
            recordTypeMapBusinessProcess[i] = BusinessProcessList[i];	// storing BusinessProcess values in map
        }
        // aspirationBusinessProcessUpdatedList will contain those data which were filled by user.
        for(var i = 0; i < listSizeBusinessProcess; i++) {
            
            if(aspirationBusinessProcessList[i].Learning_Priority__c != "Choose Priority" || aspirationBusinessProcessList[i].Your_Knowledge_Rating__c != null
               	|| aspirationBusinessProcessList[i].Support_You_Need__c != null || aspirationBusinessProcessList[i].HR_Rating__c != null ||
               	aspirationBusinessProcessList[i].Manager_Rating__c != null || aspirationBusinessProcessList[i].Add_Comment__c != null
              	|| aspirationBusinessProcessList[i].Manager_Comment__c != null || aspirationBusinessProcessList[i].HR_Comment__c != null) 
            {    
                aspirationBusinessProcessList[i].Duration__c = duration;
             	aspirationBusinessProcessList[i].Business_process__c = recordTypeMapBusinessProcess[i];	// assign Behaviour to particular skill records
                aspirationBusinessProcessUpdatedList[i] = JSON.parse(JSON.stringify(aspirationBusinessProcessList[i]));	// copy updated data to new list(aspirationSkillUpdatedList)
            }
        }
        console.log('aspirationBusinessProcessUpdatedList ' + JSON.stringify(aspirationBusinessProcessUpdatedList));
        if(aspirationBusinessProcessUpdatedList.length == 0) {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                message: 'Please enter values',
                messageTemplate: 'Please enter values',
                duration:' 5000',
                key: 'info_alt',
                type: 'error',
                mode: 'sticky'
            });
            toastEvent.fire();
            return false;
        }
         /*  Logic to find duplicate element in Learning Priority and show error message to user. Start Here*/
        updatedListSize = aspirationBusinessProcessUpdatedList.length;       // calculate length of aspirationBusinessProcessUpdatedList      
        learningPriorityArray = JSON.parse(JSON.stringify(aspirationBusinessProcessUpdatedList));
        var duplicateList = [];
        
        // storing Learning Priority entered by user in a temporary array.
        for(var i = 0, j = 0; i < updatedListSize; i++) {
            if(aspirationBusinessProcessUpdatedList[i] != null && 
               aspirationBusinessProcessUpdatedList[i].Learning_Priority__c != "Choose Priority") {
                duplicateList[j] = aspirationBusinessProcessUpdatedList[i].Learning_Priority__c;
                j++;
            }
        }	       
        duplicateList.sort();
        // to find duplicate elements in array and show toast message
        for(var i = 0; i < duplicateList.length - 1; i++) {
            if(duplicateList[i] == duplicateList[i + 1]) {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    message: 'You can not choose same learning priority for different skill',
                    messageTemplate: 'You can not choose same learning priority for different skill',
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'pester'
                });
                toastEvent.fire();
                return false;
            }
        }	
        /*  Logic to find duplicate element in Learning Priority and show error message to user. End here */
        
        //Calling the Apex Function
        var action = component.get("c.createBusinessProcessRecord"); 
        
        //Setting the Apex Parameter
        action.setParams({'aspirationBusinessProcessList' : JSON.stringify(aspirationBusinessProcessUpdatedList), 'duration': component.get("v.duration")});
        //Setting the Callback
        action.setCallback(this,function(response){
            //get the response state
            var state = response.getState();
            
            //check if result is successfull
            if(state == "SUCCESS"){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    message: 'Record is Created Successfully',
                    messageTemplate: 'Record is Created Successfully',
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'success',
                    mode: 'dismissible'
                });
                toastEvent.fire();
            } else if(state == "ERROR"){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    message: 'Error while saving records',
                    messageTemplate: 'Error while saving records',
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'pester'
                });
                toastEvent.fire();
            }
        });
        
        //adds the server-side action to the queue        
        $A.enqueueAction(action);
    },
    
    
    CoreSkillHelper : function(component,event,helper) {
        
        var updatedListSize;
		var learningPriorityArray = [];
		var aspirationCoreSkillList = component.get("v.AspirationCoreSkillList");
        var duration = component.get("v.duration");
        var listSize = aspirationCoreSkillList.length;	// calculate length of list coming from component.
        var updatedListSize;
        let aspirationCoreSkillUpdatedList = [];
        var learningPriorityArray = [];	
    
        var recordTypeMap = {};		// decalre map to store skills of CoreSkill RecordType
        var CoreSkillList = [];
        CoreSkillList = component.get("v.optionsCoreSkill");	// get picklist values from component
        var CoreSkillListSize = CoreSkillList.length;
        
        
        for(var i = 0; i < CoreSkillListSize; i++) {
            recordTypeMap[i] = CoreSkillList[i];	// storing skills values in map
        }

        // aspirationCoreSkillUpdatedList will contain those data which were filled by user.
        for(var i = 0; i < listSize; i++) {
            
            if(aspirationCoreSkillList[i].Learning_Priority__c != "Choose Priority" || aspirationCoreSkillList[i].Your_Knowledge_Rating__c != null
               	|| aspirationCoreSkillList[i].Support_You_Need__c != null || aspirationCoreSkillList[i].HR_Rating__c != null ||
               	aspirationCoreSkillList[i].Manager_Rating__c != null || aspirationCoreSkillList[i].Manager_Comment__c != null ||
              	aspirationCoreSkillList[i].HR_Comment__c != null) 
            {    
                aspirationCoreSkillList[i].Duration__c = duration;
             	aspirationCoreSkillList[i].Skills__c = recordTypeMap[i];	// assign Skills__c  to particular skill records
                aspirationCoreSkillUpdatedList[i] = JSON.parse(JSON.stringify(aspirationCoreSkillList[i]));	// copy updated data to new list(aspirationSkillUpdatedList)
            }
        }
		if(aspirationCoreSkillUpdatedList.length == 0) {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                message: 'Please enter values',
                messageTemplate: 'Please enter values',
                duration:' 5000',
                key: 'info_alt',
                type: 'error',
                mode: 'sticky'
            });
            toastEvent.fire();
            return false;
        }
         /*  Logic to find duplicate element in Learning Priority and show error message to user. Start Here*/
        updatedListSize = aspirationCoreSkillUpdatedList.length;       // calculate length of aspirationCoreSkillUpdatedList      
        learningPriorityArray = JSON.parse(JSON.stringify(aspirationCoreSkillUpdatedList));
        var duplicateList = [];
        // storing Learning Priority entered by user in a temporary array.
        for(var i = 0, j = 0; i < updatedListSize; i++) {
            if(aspirationCoreSkillUpdatedList[i] != null && 
               aspirationCoreSkillUpdatedList[i].Learning_Priority__c != "Choose Priority") {
                duplicateList[j] = aspirationCoreSkillUpdatedList[i].Learning_Priority__c;
                j++;
            }
        }	       
        duplicateList.sort();
        // to find duplicate elements in array and show toast message
        for(var i = 0; i < duplicateList.length - 1; i++) {
            if(duplicateList[i] == duplicateList[i + 1]) {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    message: 'You can not choose same learning priority for different skill',
                    messageTemplate: 'You can not choose same learning priority for different skill',
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'pester'
                });
                toastEvent.fire();
                return false;
            }
        }	
        /*  Logic to find duplicate element in Learning Priority and show error message to user. End here */
        
        //Calling the Apex Function
        var action = component.get("c.createCoreSkillRecord"); 
        
        //Setting the Apex Parameter
        action.setParams({'aspirationCoreSkillList' : JSON.stringify(aspirationCoreSkillUpdatedList), 'duration': component.get("v.duration")});
		
        //Setting the Callback
        action.setCallback(this,function(response){
            //get the response state
            var state = response.getState();
            
            //check if result is successfull
            if(state == "SUCCESS"){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    message: 'Record is Created Successfully',
                    messageTemplate: 'Record is Created Successfully',
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'success',
                    mode: 'dismissible'
                });
                toastEvent.fire();
            } else if(state == "ERROR"){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    message: 'Error while saving records',
                    messageTemplate: 'Error while saving records',
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'pester'
                });
                toastEvent.fire();
            }
        });
        
        //adds the server-side action to the queue        
        $A.enqueueAction(action);	
	},
    
    
    BehaviourHelper : function(component,event,helper) {
        
		var aspirationSkillList;
        var duration;
        var listSize;
        var updatedListSize;
        let aspirationSkillUpdatedList = [];
        var learningPriorityArray = [];				
        var recordTypeMap = {};		// decalre map to store skills of Behaviour RecordType
        var skillList = [];       
        var skillListSize;
        var duplicateList = [];
        
        
        skillList = component.get("v.options");	// get picklist values from component
        skillListSize = skillList.length;
        aspirationSkillList = component.get("v.AspirationSkill");
        duration = component.get("v.duration");
        listSize = aspirationSkillList.length;	// calculate length of list coming from component.
        
        for(var i = 0; i < skillListSize; i++) {
            recordTypeMap[i] = skillList[i];	// storing skills values in map
        }
		console.log('recordTypeMap ' + recordTypeMap);
        // aspirationSkillUpdatedList will contain those data which were filled by user.
        for(var i = 0; i < listSize; i++) {
            
            if(aspirationSkillList[i].Learning_Priority__c != "Choose Priority" || 
               	aspirationSkillList[i].Your_Knowledge_Rating__c != null
               	|| aspirationSkillList[i].Support_You_Need__c != null || aspirationSkillList[i].HR_Rating__c != null ||
               	aspirationSkillList[i].Manager_Rating__c != null || aspirationSkillList[i].Manager_Comment__c != null ||
              	aspirationSkillList[i].HR_Comment__c != null) 
            {    
                aspirationSkillList[i].Duration__c = duration;
             	aspirationSkillList[i].Behaviour__c = recordTypeMap[i];	// assign Behaviour to particular skill records
                aspirationSkillUpdatedList[i] = JSON.parse(JSON.stringify(aspirationSkillList[i]));	// copy updated data to new list(aspirationSkillUpdatedList)
            }
        }
        if(aspirationSkillUpdatedList.length == 0) {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                message: 'Please enter values',
                messageTemplate: 'Please enter values',
                duration:' 5000',
                key: 'info_alt',
                type: 'error',
                mode: 'sticky'
            });
            toastEvent.fire();
            return false;
        }
        console.log('aspirationSkillUpdatedList ' + JSON.stringify(aspirationSkillUpdatedList));
        /*  Logic to find duplicate element in Learning Priority and show error message to user. Start Here*/
        updatedListSize = aspirationSkillUpdatedList.length;       // calculate length of aspirationSkillUpdatedList      
        learningPriorityArray = JSON.parse(JSON.stringify(aspirationSkillUpdatedList));
        
        // storing Learning Priority entered by user in a temporary array.
        for(var i = 0, j = 0; i < updatedListSize; i++) {
            if(aspirationSkillUpdatedList[i] != null && 
               aspirationSkillUpdatedList[i].Learning_Priority__c != "Choose Priority") {
                duplicateList[j] = aspirationSkillUpdatedList[i].Learning_Priority__c;
                j++;
            }
        }	       
        duplicateList.sort();
        console.log('duplicateList ' + duplicateList);
        // to find duplicate elements in array and show toast message
        for(var i = 0; i < duplicateList.length - 1; i++) {
            if(duplicateList[i] == duplicateList[i + 1]) {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    message: 'You can not choose same learning priority for different skill',
                    messageTemplate: 'You can not choose same learning priority for different skill',
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'pester'
                });
                toastEvent.fire();
                return false;
            }
        }	
        /*  Logic to find duplicate element in Learning Priority and show error message to user. End here */
        
        //Calling the Apex Function
        var action = component.get("c.createBehaviourRecord"); 
        
        //Setting the Apex Parameter
        action.setParams({'aspirationSkillList' : JSON.stringify(aspirationSkillUpdatedList), 'duration': component.get("v.duration")});
		
        //Setting the Callback
        action.setCallback(this,function(response){
            //get the response state
            var state = response.getState();
            
            //check if result is successfull
            if(state == "SUCCESS"){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    message: 'Record is Created Successfully',
                    messageTemplate: 'Record is Created Successfully',
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'success',
                    mode: 'dismissible'
                });
                toastEvent.fire();
            } else if(state == "ERROR"){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    message: 'Error while saving records',
                    messageTemplate: 'Error while saving records',
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'pester'
                });
                toastEvent.fire();
            }
        });
        
        //adds the server-side action to the queue        
        $A.enqueueAction(action);	
	},
    
   

	IndustryHelper : function(component,event,helper) {
        var updatedListSize;
		var learningPriorityArray = [];
		var aspirationIndustryList;
        var duration;
        var listSizeIndustry;
        var updatedListSizeIndustry;
        let aspirationIndustryUpdatedList = [];
        var learningPriorityArrayIndustry = [];       
        var recordTypeMapIndustry = {};		// decalre map to store skills of TrailHead RecordType
        var IndustryList = [];
        var duplicateList = [];        
        var IndustryListSize;
        
       	
        IndustryList = component.get("v.optionsIndustry");	// get picklist values from component
        IndustryListSize = IndustryList.length;
        aspirationIndustryList = component.get("v.AspirationIndustryList");
        duration = component.get("v.duration");
        listSizeIndustry = aspirationIndustryList.length;	// calculate length of list coming from component.
        
        for(var i = 0; i < IndustryListSize; i++) {
            recordTypeMapIndustry[i] = IndustryList[i];	// storing Industry values in map
        }
        console.log('recordTypeMapIndustry ' + recordTypeMapIndustry);
        // aspirationIndustryUpdatedList will contain those data which were filled by user.
        for(var i = 0; i < listSizeIndustry; i++) {
            
            if(aspirationIndustryList[i].Learning_Priority__c != "Choose Priority" || 
               	aspirationIndustryList[i].Your_Knowledge_Rating__c != null
               	|| aspirationIndustryList[i].Support_You_Need__c != null || aspirationIndustryList[i].HR_Rating__c != null ||
               	aspirationIndustryList[i].Manager_Rating__c != null || aspirationIndustryList[i].Add_Comment__c != null
              	|| aspirationIndustryList[i].Manager_Comment__c != null || aspirationIndustryList[i].HR_Comment__c != null) 
            {    
                aspirationIndustryList[i].Duration__c = duration;
             	aspirationIndustryList[i].Industry__c = recordTypeMapIndustry[i];	// assign TrailHead to particular skill records
                aspirationIndustryUpdatedList[i] = JSON.parse(JSON.stringify(aspirationIndustryList[i]));	// copy updated data to new list(aspirationSkillUpdatedList)
            }
        }
        if(aspirationIndustryUpdatedList.length == 0) {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                message: 'Please enter values',
                messageTemplate: 'Please enter values',
                duration:' 5000',
                key: 'info_alt',
                type: 'error',
                mode: 'sticky'
            });
            toastEvent.fire();
            return false;
        }
        console.log('aspirationIndustryUpdatedList ' + JSON.stringify(aspirationIndustryUpdatedList));
         /*  Logic to find duplicate element in Learning Priority and show error message to user. Start Here*/
        updatedListSize = aspirationIndustryUpdatedList.length;       // calculate length of aspirationIndustryUpdatedList      
        learningPriorityArray = JSON.parse(JSON.stringify(aspirationIndustryUpdatedList));
        
        // storing Learning Priority entered by user in a temporary array.
        for(var i = 0, j = 0; i < updatedListSize; i++) {
            if(aspirationIndustryUpdatedList[i] != null && 
               aspirationIndustryUpdatedList[i].Learning_Priority__c != "Choose Priority") {
                duplicateList[j] = aspirationIndustryUpdatedList[i].Learning_Priority__c;
                j++;
            }
        }	       
        duplicateList.sort();
        console.log('duplicateList ' + duplicateList);
        // to find duplicate elements in array and show toast message
        for(var i = 0; i < duplicateList.length - 1; i++) {
            if(duplicateList[i] == duplicateList[i + 1]) {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    message: 'You can not choose same learning priority for different skill',
                    messageTemplate: 'You can not choose same learning priority for different skill',
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'pester'
                });
                toastEvent.fire();
                return false;
            }
        }	
        /*  Logic to find duplicate element in Learning Priority and show error message to user. End here */
        
        //Calling the Apex Function
        var action = component.get("c.createIndustryRecord"); 
        
        //Setting the Apex Parameter
        action.setParams({'aspirationIndustryList' : JSON.stringify(aspirationIndustryUpdatedList), 'duration': component.get("v.duration")});
		
        //Setting the Callback
        action.setCallback(this,function(response){
            
            //get the response state
            var state = response.getState();
            
            //check if result is successfull
            if(state == "SUCCESS"){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    message: 'Record is Created Successfully',
                    messageTemplate: 'Record is Created Successfully',
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'success',
                    mode: 'dismissible'
                });
                toastEvent.fire();
            } else if(state == "ERROR"){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    message: 'Error while saving records',
                    messageTemplate: 'Error while saving records',
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'pester'
                });
                toastEvent.fire();
            }
        });
        
        //adds the server-side action to the queue        
        $A.enqueueAction(action);
	},
       
    SFProductHelper : function(component,event,helper) {
        var updatedListSize;
		var learningPriorityArray = [];
		var aspirationSFProductList;
        var duration;
        var listSize;
        var updatedListSize;
        let aspirationSFProductUpdatedList = [];
        var learningPriorityArray = [];	    
        var recordTypeMap = {};		// decalre map to store skills of SFProduct RecordType
        var SFProductList = [];
        var duplicateList = [];       
        var SFProductListSize;
        
        duration = component.get("v.duration");
        aspirationSFProductList = component.get("v.AspirationSFProductList");
        listSize = aspirationSFProductList.length;	// calculate length of list coming from component.
        SFProductList = component.get("v.optionsSFProduct");	// get picklist values from component
        SFProductListSize = SFProductList.length;
        
        for(var i = 0; i < SFProductListSize; i++) {
            recordTypeMap[i] = SFProductList[i];	// storing skills values in map
        }
		console.log('recordTypeMap ' + recordTypeMap);
        // aspirationSFProductUpdatedList will contain those data which were filled by user.
        for(var i = 0; i < listSize; i++) {
            
            if(aspirationSFProductList[i].Learning_Priority__c != "Choose Priority" || 
               	aspirationSFProductList[i].Your_Knowledge_Rating__c != null
               	|| aspirationSFProductList[i].Support_You_Need__c != null || aspirationSFProductList[i].HR_Rating__c != null ||
               	aspirationSFProductList[i].Manager_Rating__c != null || aspirationSFProductList[i].Manager_Comment__c != null ||
              	aspirationSFProductList[i].HR_Comment__c != null) 
            {    
                aspirationSFProductList[i].Duration__c = duration;
             	aspirationSFProductList[i].Product__c = recordTypeMap[i];	// assign Product__c  to particular skill records
                aspirationSFProductUpdatedList[i] = JSON.parse(JSON.stringify(aspirationSFProductList[i]));	// copy updated data to new list(aspirationSkillUpdatedList)
            }
        }
        if(aspirationSFProductUpdatedList.length == 0) {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                message: 'Please enter values',
                messageTemplate: 'Please enter values',
                duration:' 5000',
                key: 'info_alt',
                type: 'error',
                mode: 'sticky'
            });
            toastEvent.fire();
            return false;
        }
		console.log('aspirationSFProductUpdatedList ' + JSON.stringify(aspirationSFProductUpdatedList));
         /*  Logic to find duplicate element in Learning Priority and show error message to user. Start Here*/
        updatedListSize = aspirationSFProductUpdatedList.length;       // calculate length of aspirationSFProductUpdatedList      
        learningPriorityArray = JSON.parse(JSON.stringify(aspirationSFProductUpdatedList));
        
        // storing Learning Priority entered by user in a temporary array.
        for(var i = 0, j = 0; i < updatedListSize; i++) {
            if(aspirationSFProductUpdatedList[i] != null && 
               aspirationSFProductUpdatedList[i].Learning_Priority__c != "Choose Priority") {
                duplicateList[j] = aspirationSFProductUpdatedList[i].Learning_Priority__c;
                j++;
            }
        }	       
        duplicateList.sort();
        console.log('duplicateList ' + duplicateList);
        // to find duplicate elements in array and show toast message
        for(var i = 0; i < duplicateList.length - 1; i++) {
            if(duplicateList[i] == duplicateList[i + 1]) {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    message: 'You can not choose same learning priority for different skill',
                    messageTemplate: 'You can not choose same learning priority for different skill',
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'pester'
                });
                toastEvent.fire();
                return false;
            }
        }	
        /*  Logic to find duplicate element in Learning Priority and show error message to user. End here */
        
        //Calling the Apex Function
        var action = component.get("c.createSFProductRecord"); 
        
        //Setting the Apex Parameter
        action.setParams({'aspirationSFProductList' : JSON.stringify(aspirationSFProductUpdatedList), 'duration': component.get("v.duration")});
		
        //Setting the Callback
        action.setCallback(this,function(response){
            //get the response state
            var state = response.getState();
            
            //check if result is successfull
            if(state == "SUCCESS"){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    message: 'Record is Created Successfully',
                    messageTemplate: 'Record is Created Successfully',
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'success',
                    mode: 'dismissible'
                });
                toastEvent.fire();
            } else if(state == "ERROR"){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    message: 'Error while saving records',
                    messageTemplate: 'Error while saving records',
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'pester'
                });
                toastEvent.fire();
            }
        });
        
        //adds the server-side action to the queue        
        $A.enqueueAction(action);	
	},
    
    AspPersonalHelper : function(component,event,helper) {
        
        var aspirationPersonalList = component.get("v.AspirationAspPersonalList");
        var listSize = aspirationPersonalList.length;	// calculate length of list coming from component.
        var duration = component.get("v.duration");
        var aspirationPersonalUpdatedList = [];
        
        // aspirationSFProductUpdatedList will contain those data which were filled by user.
        for(var i = 0; i < listSize; i++) {
            
            if(aspirationPersonalList[i].Describe_yourself_in_few_words__c != null || 
               	aspirationPersonalList[i].Short_Term_Personal_Goals__c != null
               	|| aspirationPersonalList[i].Long_Term_Personal_Goals__c != null || 
               	aspirationPersonalList[i].What_activities_give_you_joy_excitement__c != null ||
               	aspirationPersonalList[i].Support_require_to_realize_your_goals__c != null || 
               	aspirationPersonalList[i].Changes_you_want_to_yourself__c != null) 
            {    
                aspirationPersonalList[i].Duration__c = duration;
                aspirationPersonalUpdatedList[i] = JSON.parse(JSON.stringify(aspirationPersonalList[i]));	// copy updated data to new list(aspirationSkillUpdatedList)
            }
        }
        if(aspirationPersonalUpdatedList.length == 0) {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                message: 'Please enter values',
                messageTemplate: 'Please enter values',
                duration:' 5000',
                key: 'info_alt',
                type: 'error',
                mode: 'sticky'
            });
            toastEvent.fire();
            return false;
        }
        console.log('aspirationPersonalUpdatedList ' + JSON.stringify(aspirationPersonalUpdatedList));
        //Calling the Apex Function
        var action = component.get("c.createAspPersonalRecord");         
        //Setting the Apex Parameter
        action.setParams({'aspirationPersonalList' : JSON.stringify(aspirationPersonalUpdatedList), 'duration': component.get("v.duration")});
		
        //Setting the Callback
        action.setCallback(this,function(response){
            //get the response state
            var state = response.getState();
            
            //check if result is successfull
            if(state == "SUCCESS"){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    message: 'Record is Created Successfully',
                    messageTemplate: 'Record is Created Successfully',
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'success',
                    mode: 'dismissible'
                });
                toastEvent.fire();
            } else if(state == "ERROR"){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    message: 'Error while saving records',
                    messageTemplate: 'Error while saving records',
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'pester'
                });
                toastEvent.fire();
            }
        });
        
        //adds the server-side action to the queue        
        $A.enqueueAction(action);	
    },
    
    AspProfessionalHelper : function(component,event,helper) {
        
        var aspirationProfessionalList = component.get("v.AspirationAspProfessionalList");
        var listSize = aspirationProfessionalList.length;	// calculate length of list coming from component.
        var duration = component.get("v.duration");
        var aspirationProfessionalUpdatedList = [];
        
        // aspirationSFProductUpdatedList will contain those data which were filled by user.
        for(var i = 0; i < listSize; i++) {
            
            if(aspirationProfessionalList[i].Type_Of_Project_You_Want_To_Work__c != null || 
               	aspirationProfessionalList[i].What_Is_Exciting_In_Your_Project__c != null
               	|| aspirationProfessionalList[i].What_activities_give_you_joy_excitement__c != null || 
               	aspirationProfessionalList[i].Top_3_Things_You_Want_To_Learn__c != null ||
               	aspirationProfessionalList[i].Support_require_to_realize_your_goals__c != null || 
               	aspirationProfessionalList[i].Where_Do_You_See_Yourself_in_3_Years__c != null) 
            {    
                aspirationProfessionalList[i].Duration__c = duration;
                aspirationProfessionalUpdatedList[i] = JSON.parse(JSON.stringify(aspirationProfessionalList[i]));	// copy updated data to new list(aspirationSkillUpdatedList)
            }
        }
        if(aspirationProfessionalUpdatedList.length == 0) {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                message: 'Please enter values',
                messageTemplate: 'Please enter values',
                duration:' 5000',
                key: 'info_alt',
                type: 'error',
                mode: 'sticky'
            });
            toastEvent.fire();
            return false;
        }
        console.log('aspirationProfessionalUpdatedList ' + JSON.stringify(aspirationProfessionalUpdatedList));
        //Calling the Apex Function
        var action = component.get("c.createAspProfessionalRecord");         
        //Setting the Apex Parameter
        action.setParams({'aspirationProfessionalList' : JSON.stringify(aspirationProfessionalUpdatedList), 'duration': component.get("v.duration")});
		
        //Setting the Callback
        action.setCallback(this,function(response){
            //get the response state
            var state = response.getState();
            
            //check if result is successfull
            if(state == "SUCCESS"){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    message: 'Record is Created Successfully',
                    messageTemplate: 'Record is Created Successfully',
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'success',
                    mode: 'dismissible'
                });
                toastEvent.fire();
            } else if(state == "ERROR"){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    message: 'Error while saving records',
                    messageTemplate: 'Error while saving records',
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'pester'
                });
                toastEvent.fire();
            }
        });
        
        //adds the server-side action to the queue        
        $A.enqueueAction(action);	
    },
    
    THHelper : function(component,event,helper) {
        
        var aspirationTHList = component.get("v.AspirationTHList");
        var listSize = aspirationTHList.length;	// calculate length of list coming from component.
        var duration = component.get("v.duration");
        var aspirationTHUpdatedList = [];
        
        // aspirationSFProductUpdatedList will contain those data which were filled by user.
        for(var i = 0; i < listSize; i++) {
            
            if(aspirationTHList[i].Plan_For_the_Month__c != "None" || 
               	aspirationTHList[i].of_trails_I_plan_to_achieve_this_month__c != null
               	|| aspirationTHList[i].of_points_I_plan_to_achieve_this_month__c != null || 
               	aspirationTHList[i].of_badges_I_plan_to_achieve_this_month__c != null ||
               	aspirationTHList[i].Name_of_the_badges_I_plan_to_achieve__c != null || 
               	aspirationTHList[i].of_trail_I_completed_in_this_month__c != null
              	|| aspirationTHList[i].of_points_I_completed_in_this_month__c != null || 
               	aspirationTHList[i].Status__c != "None" ||
               	aspirationTHList[i].Name_the_badges_completed_in_this_month__c != null || 
               	aspirationTHList[i].of_badges_I_completed_in_this_month__c != null
              	|| aspirationTHList[i].My_trail_head_page__c != null || aspirationTHList[i].Reason_for_the_gap__c != null) 
            {    
                aspirationTHList[i].Duration__c = duration;
                aspirationTHUpdatedList[i] = JSON.parse(JSON.stringify(aspirationTHList[i]));	// copy updated data to new list(aspirationSkillUpdatedList)
            }
        }
        console.log('aspirationTHUpdatedList ' + aspirationTHUpdatedList.length);
        if(aspirationTHUpdatedList.length == 0) {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                message: 'Please enter values',
                messageTemplate: 'Please enter values',
                duration:' 5000',
                key: 'info_alt',
                type: 'error',
                mode: 'sticky'
            });
            toastEvent.fire();
            return false;
        }
        console.log('aspirationTHUpdatedList ' + JSON.stringify(aspirationTHUpdatedList));
        //Calling the Apex Function
        var action = component.get("c.createTHRecord");         
        //Setting the Apex Parameter
        action.setParams({'aspirationTHList' : JSON.stringify(aspirationTHUpdatedList), 'duration': component.get("v.duration")});
		
        //Setting the Callback
        action.setCallback(this,function(response){
            //get the response state
            var state = response.getState();
            
            //check if result is successfull
            if(state == "SUCCESS"){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    message: 'Record is Created Successfully',
                    messageTemplate: 'Record is Created Successfully',
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'success',
                    mode: 'dismissible'
                });
                toastEvent.fire();
            } else if(state == "ERROR"){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    message: 'Error while saving records',
                    messageTemplate: 'Error while saving records',
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'pester'
                });
                toastEvent.fire();
            }
        });
        
        //adds the server-side action to the queue        
        $A.enqueueAction(action);	
    },
    
    CertificateRecordHelper : function(component,event,helper) {
        var aspirationTHList = component.get("v.AspirationCertRecordList");
        var listSize = aspirationTHList.length;	// calculate length of list coming from component.
        var duration = component.get("v.duration");
        var aspirationTHUpdatedList = [];
        console.log('aspirationTHList ' + JSON.stringify(aspirationTHList));
        // aspirationSFProductUpdatedList will contain those data which were filled by user.
        for(var i = 0; i < listSize; i++) {
            
            if(aspirationTHList[i].Certifications__c != "None" || aspirationTHList[i].Other_Certifications__c != null
               	|| aspirationTHList[i].Is_It_Linked_to_KVP_Portal__c != null || aspirationTHList[i].Certification_valid_up_to__c != null ||
               	aspirationTHList[i].Webassesor_link__c != null || aspirationTHList[i].Approval_Status__c != "None") 
            {    
                if(aspirationTHList[i].Is_It_Linked_to_KVP_Portal__c != true) {
                    aspirationTHList[i].Is_It_Linked_to_KVP_Portal__c = false;
                    aspirationTHList[i].Duration__c = duration;
                	aspirationTHUpdatedList[i] = JSON.parse(JSON.stringify(aspirationTHList[i]));	// copy updated data to new list(aspirationSkillUpdatedList)
                } else {
                    aspirationTHList[i].Duration__c = duration;
                	aspirationTHUpdatedList[i] = JSON.parse(JSON.stringify(aspirationTHList[i]));	// copy updated data to new list(aspirationSkillUpdatedList)
                }
                
            }
        }
        console.log('aspirationTHUpdatedList length ' + aspirationTHUpdatedList.length);
        
        // check if user doesn't enter any data and click on save button
        if(aspirationTHUpdatedList.length == 0) {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                message: 'Please enter values',
                messageTemplate: 'Please enter values',
                duration:' 5000',
                key: 'info_alt',
                type: 'error',
                mode: 'sticky'
            });
            toastEvent.fire();
            return false;
        }
        console.log('aspirationTHUpdatedList ' + JSON.stringify(aspirationTHUpdatedList));
        
        // first call the helper function in if block which will return true or false.
        // this helper function check the "Mandatory field" will not be blank.
        if(helper.validateRequired(component, event, aspirationTHUpdatedList)) {
            //Calling the Apex Function
            var action = component.get("c.createCertificateRecord");         
            //Setting the Apex Parameter
            action.setParams({'aspirationCertRecordList' : JSON.stringify(aspirationTHUpdatedList), 'duration': component.get("v.duration")});
            
            //Setting the Callback
            action.setCallback(this,function(response){
                //get the response state
                var state = response.getState();
                
                //check if result is successfull
                if(state == "SUCCESS") {
                    console.log(' state SUCCESS');
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        message: 'Record is Created Successfully',
                        messageTemplate: 'Record is Created Successfully',
                        duration:' 5000',
                        key: 'info_alt',
                        type: 'success',
                        mode: 'dismissible'
                    });
                    toastEvent.fire();
                } else if(state == "ERROR") {
                    console.log('Errors ' + response.getError());
                    console.log('Errors ' + JSON.stringify(response.getError()));
                    $A.log("Errors using $A", response.getError());
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        message: 'Please Specify If Others is mandatory field',
                        messageTemplate: 'Please Specify If Others is mandatory field',
                        duration:' 5000',
                        key: 'info_alt',
                        type: 'error',
                        mode: 'pester'
                    });
                    toastEvent.fire();
                }
            });
            
            //adds the server-side action to the queue        
            $A.enqueueAction(action);
        }	
    },
    // helper function for check if Mandatory field is not null/blank on save  
    validateRequired: function(component, event, aspirationTHUpdatedList) {
        var isValid = true;
        if (aspirationTHUpdatedList[0].Certifications__c == 'None') {
            isValid = false;
          //  alert('Certifications Can not be Blank');
            var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    message: 'Certifications Can not be Blank',
                    messageTemplate: 'Certifications Can not be Blank',
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'pester'
                });
                toastEvent.fire();
        }
        return isValid;
    },
    
    CertificateApplyHelper : function(component,event,helper) {
        
        var AspirationCertApplyList = component.get("v.AspirationCertApplyList");
        var listSize = AspirationCertApplyList.length;	// calculate length of list coming from component.
        var duration = component.get("v.duration");
        var aspirationCerApplyUpdatedList = [];
        console.log('AspirationCertApplyList ' + JSON.stringify(AspirationCertApplyList));
        // aspirationSFProductUpdatedList will contain those data which were filled by user.
        for(var i = 0; i < listSize; i++) {
            
            if(AspirationCertApplyList[i].Credentials__c != "None" || AspirationCertApplyList[i].Certifications__c != "None"
               	|| AspirationCertApplyList[i].Other_Certifications__c != null ||
               	AspirationCertApplyList[i].Certification_Status__c != "None" || AspirationCertApplyList[i].Date_of_Certification__c != null ||
              	AspirationCertApplyList[i].Credential_Overview_Link__c != null || AspirationCertApplyList[i].Support_You_Need__c != null ||
               	AspirationCertApplyList[i].Steps_You_Plan_to_Take_to_Improve__c != null || AspirationCertApplyList[i].Your_Knowledge_Rating__c != null) 
            {    
             
                AspirationCertApplyList[i].Duration__c = duration;
                aspirationCerApplyUpdatedList[i] = JSON.parse(JSON.stringify(AspirationCertApplyList[i]));	// copy updated data to new list(aspirationSkillUpdatedList)
            }
        }
        
        if(aspirationCerApplyUpdatedList.length == 0) {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                message: 'Please enter values',
                messageTemplate: 'Please enter values',
                duration:' 5000',
                key: 'info_alt',
                type: 'error',
                mode: 'sticky'
            });
            toastEvent.fire();
            return false;
        }
        console.log('aspirationCerApplyUpdatedList ' + JSON.stringify(aspirationCerApplyUpdatedList));
        
        //Calling the Apex Function
        var action = component.get("c.createCerApplyRecord");         
        //Setting the Apex Parameter
        action.setParams({'AspirationCertApplyList' : JSON.stringify(aspirationCerApplyUpdatedList), 'duration': component.get("v.duration")});
		
        //Setting the Callback
        action.setCallback(this,function(response){
            //get the response state
            var state = response.getState();
            
            //check if result is successfull
            if(state == "SUCCESS"){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    message: 'Record is Created Successfully',
                    messageTemplate: 'Record is Created Successfully',
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'success',
                    mode: 'dismissible'
                });
                toastEvent.fire();
            } else if(state == "ERROR"){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    message: 'Please fill mandatory fields',
                    messageTemplate: 'Please fill mandatory fields',
                    duration:' 5000',
                    key: 'info_alt',
                    type: 'error',
                    mode: 'pester'
                });
                toastEvent.fire();
            }
        });
        //adds the server-side action to the queue        
        $A.enqueueAction(action);	
    },
    
    
    // function to check for which duration(H1/H2) user will insert records
    displayDurationHelper : function(component,event,helper) {
		
        //Calling the Apex Function
        var action = component.get("c.getDisplayDuration"); 
       
        //Setting the Callback
        action.setCallback(this,function(response){
            //get the response state
            var state = response.getState();
            
            //check if result is successfull
            if(state == "SUCCESS") {
                var allLabelName = [];
                allLabelName = response.getReturnValue(); 
                var H1Label, H2Label, H3Label; 
                H1Label = allLabelName[0];
                H2Label = allLabelName[1];
                H3Label = allLabelName[2];
                
                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth()+1; //January is 0!
                var yyyy = today.getFullYear();
                
                if(dd<10) {
                    dd = '0'+dd
                } 
                
                if(mm<10) {
                    mm = '0'+mm
                }                
                today = mm + '/' + dd + '/' + yyyy;
                if(today > H1Label && today < H2Label) {
                    component.set("v.duration", 'H1');
                    component.set("v.displayToggleMessage", 'You are filling records for H1 (Jan ' + yyyy + ' - June ' + yyyy + ') period');
                }
                
                if(today > H2Label && today < H3Label) {
                    component.set("v.duration", 'H2');
                    component.set("v.displayToggleMessage", 'You are filling records for H2 (Jul ' + yyyy + ' - Dec ' + yyyy + ') period');
                }
            } 
        });
        
        //adds the server-side action to the queue        
        $A.enqueueAction(action);	
    },
})