({
    searchHelper : function(component,event,getInputkeyWord) {
        var action = component.get("c.fetchLookUpValues");
        action.setParams({
            'searchKeyWord': getInputkeyWord,
            'ObjectName' : component.get("v.objectAPIName")
        });
        action.setCallback(this, function(response) {
            $A.util.removeClass(component.find("mySpinner"), "slds-show");
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                if (storeResponse.length == 0) {
                    component.set("v.Message", 'No Result Found...');
                } else {
                    component.set("v.Message", '');
                }
                component.set("v.listOfSearchRecords", storeResponse);
            }
        });
        $A.enqueueAction(action);
    },
    searchUserQueue  : function(component,event,getInputkeyWord) {
        var action = component.get("c.fetchUserQueueValues");
        action.setParams({
            'searchKeyWord': getInputkeyWord,
        });
        action.setCallback(this, function(response) {
            $A.util.removeClass(component.find("mySpinner"), "slds-show");
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                if (storeResponse.length == 0) {
                    component.set("v.Message", 'No Result Found...');
                } else {
                    component.set("v.Message", '');
                }
                component.set("v.listOfSearchRecords", storeResponse);
            }
        });
        $A.enqueueAction(action);
    },
    getKPIDetailsHelper : function(component,event,helper){
        var csatValues=component.get('v.CSATValue');
        var project=component.get('v.selectedRecord');
        //alert('calling alert of helper' +csatValues);
        var action = component.get("c.getCSATValues");
        //alert(project.Name);
        action.setParams({
            'projectId': project.Id,
        });
        
        
        
        
        action.setCallback(this, function(response) {
            $A.util.removeClass(component.find("mySpinner"), "slds-show");
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                /*if (storeResponse.length == 0) {
                    component.set("v.Message", 'No Result Found...');
                } else {
                    component.set("v.Message", '');
                }*/
                component.set("v.CSATValue", storeResponse[0][0])
                component.set("v.projectScope", storeResponse[0][1]);
                component.set("v.projectSchedule", storeResponse[0][2]);
                component.set("v.taskSchedule", storeResponse[0][3]);  
                component.set("v.qEffort", storeResponse[0][4]);
                component.set("v.costOfQuality", storeResponse[0][5]); 
                component.set("v.budget", storeResponse[0][6]);
                component.set("v.documents", storeResponse[0][7]);
                component.set("v.onTimeInvoice", storeResponse[0][8]);
                component.set("v.gMargin", storeResponse[0][10]);
                
                component.set("v.CSATValueActual", storeResponse[1][0])
                component.set("v.projectScopeActual", storeResponse[1][1]);
                component.set("v.projectScheduleActual", storeResponse[1][2]);
                component.set("v.taskScheduleActual", storeResponse[1][3]);  
                component.set("v.qEffortActual", storeResponse[1][4]);
                component.set("v.costOfQualityActual", storeResponse[1][5]); 
                component.set("v.budgetActual", storeResponse[1][6]);
                component.set("v.documentsActual", storeResponse[1][7]);
                component.set("v.onTimeInvoiceActual", storeResponse[1][8]);
                component.set("v.gMarginActual", storeResponse[1][10]);
                var total=0;
                for( var i=0;i<10;i++){
                    total+=storeResponse[0][i];
                }
                document.getElementById("myProgress").value = total;
                
                var Ptg=(total*100)/750;
                document.getElementById("totalVal").style.width=Ptg+"%";
                document.getElementById("totalVal").style.color="#086b08";
                document.getElementById("totalVal").style.fontSize ="16px";
                document.getElementById("totalVal").style.fontWeight="bold";
                document.getElementById("totalVal").innerHTML=total;
                var totalValue=750;
                var twentyperc=(20*totalValue)/100;
                var fourthyperc=(40*totalValue)/100;
                var sixthyperc=(60*totalValue)/100;
                var eigthyperc=(80*totalValue)/100;
                if(total>=twentyperc) {
                    document.querySelector(".onestar").style.color="yellow";
                    console.log(twentyperc);
                    console.log(total);
                }
                if(total>=fourthyperc) {
                   
                        document.querySelector(".twostar").style.color="yellow";
                    console.log(twentyperc);
                }
                if(total>=sixthyperc) {
                    for(var i=1; i<=3; i++)
                        document.querySelector(".threestar").style.color="yellow";
                }
                if(total>=eigthyperc) {
                    for(var i=1; i<=4; i++)
                        document.querySelector(".fourstar").style.color="yellow";
                }
                if(total>=totalValue) {
                    for(var i=1; i<=5; i++)
                        document.querySelector(".fivestar").style.color="yellow";
                }
                
                if(total>=twentyperc) {
                    document.querySelector(".onestar").style.color="yellow";
                    console.log(twentyperc);
                    console.log(total);
                }
                 if(total>=fourthyperc) {
                     for(var i=1; i<=2; i++)
                    document.querySelector(".overall_"+i).style.color="yellow";
                     console.log(twentyperc);
                }
                if(total>=sixthyperc) {
                     for(var i=1; i<=3; i++)
                    document.querySelector(".overall_"+i).style.color="yellow";
                }
                if(total>=eigthyperc) {
                     for(var i=1; i<=4; i++)
                    document.querySelector(".overall_"+i).style.color="yellow";
                }
                if(total>=totalValue) {
                     for(var i=1; i<=5; i++)
                    document.querySelector(".overall_"+i).style.color="yellow";
                }
                //alert(storeResponse[9])
                //alert('Test Data'+storeResponse);
                
                //    helper.getDetails(component,event,helper);
                
                
                
            }
        });
        $A.enqueueAction(action);
    },
    
    getDetails : function(component,event,helper){
        var action = component.get('c.getCalculatedData');
        action.setCallback(this,function(response){
            var state = response.getState();
            var result={};
            if(state==='SUCCESS'){
                result = response.getReturnValue();
                console.log('333333gfgf3 ' + result.length);
                console.log(result);
                console.log(typeof result);
                component.set('v.showDetailsMap',result);
                console.log(typeof component.get('v.showDetailsMap'))
                
                console.log(component.get('v.showDetailsMap')+ ' Testing Map Values');
                //component.set('v.showDetails',result[1]);    
            }
        });
        $A.enqueueAction(action);
    },
})