({
    onfocus : function(component,event,helper){
        $A.util.addClass(component.find("mySpinner"), "slds-show");
        var forOpen = component.find("searchRes");
        $A.util.addClass(forOpen, 'slds-is-open');
        $A.util.removeClass(forOpen, 'slds-is-close');
        var getInputkeyWord = '';
        helper.searchHelper(component,event,getInputkeyWord);
    },
    onblur : function(component,event,helper){       
        component.set("v.listOfSearchRecords", null );
        var forclose = component.find("searchRes");
        $A.util.addClass(forclose, 'slds-is-close');
        $A.util.removeClass(forclose, 'slds-is-open');
    },
    keyPressController : function(component, event, helper) {
        var getInputkeyWord = component.get("v.SearchKeyWord");
        var objectName = component.get('v.objectAPIName');   
        if( getInputkeyWord.length > 0 ){
            var forOpen = component.find("searchRes");
            $A.util.addClass(forOpen, 'slds-is-open');
            $A.util.removeClass(forOpen, 'slds-is-close');
            if(objectName == 'UserQueue') {
                helper.searchUserQueue(component, event, getInputkeyWord);
            } else {
                helper.searchHelper(component, event, getInputkeyWord);
            }
        }
        else{  
            component.set("v.listOfSearchRecords", null ); 
            var forclose = component.find("searchRes");
            $A.util.addClass(forclose, 'slds-is-close');
            $A.util.removeClass(forclose, 'slds-is-open');
        }
    },
    // function for clear the Record Selaction 
    clear :function(component,event,heplper){
        var pillTarget = component.find("lookup-pill");
        var lookUpTarget = component.find("lookupField"); 
        
        $A.util.addClass(pillTarget, 'slds-hide');
        $A.util.removeClass(pillTarget, 'slds-show');
        
        $A.util.addClass(lookUpTarget, 'slds-show');
        $A.util.removeClass(lookUpTarget, 'slds-hide');
        
        component.set("v.SearchKeyWord",null);
        component.set("v.listOfSearchRecords", null );
        component.set("v.selectedRecord", {} );
        component.set("v.listOfSearchRecords",null);
    },
    // This function call when the end User Select any record from the result list.   
    handleComponentEvent : function(component, event, helper) {
        // get the selected Account record from the COMPONETN event 	 
        var selectedAccountGetFromEvent = event.getParam("recordByEvent");
        component.set("v.selectedRecord" , selectedAccountGetFromEvent); 
        //alert(selectedAccountGetFromEvent.Name);
        component.set("v.CSATValue",null)
        component.set("v.projectScope", null);
        component.set("v.projectSchedule", null);
        component.set("v.taskSchedule", null);  
        component.set("v.qEffort", null);
        component.set("v.costOfQuality", null); 
        component.set("v.budget", null);
        component.set("v.documents", null);
        component.set("v.onTimeInvoice", null);
        component.set("v.gMargin", null);
        
        component.set("v.CSATValueActual", null)
        component.set("v.projectScopeActual", null);
        component.set("v.projectScheduleActual",null);
        component.set("v.taskScheduleActual", null);  
        component.set("v.qEffortActual", null);
        component.set("v.costOfQualityActual", null); 
        component.set("v.budgetActual", null);
        component.set("v.documentsActual", null);
        component.set("v.onTimeInvoiceActual", null);
        component.set("v.gMarginActual", null);
        
        var forclose = component.find("lookup-pill");
        $A.util.addClass(forclose, 'slds-show');
        $A.util.removeClass(forclose, 'slds-hide');
        
        var forclose = component.find("searchRes");
        $A.util.addClass(forclose, 'slds-is-close');
        $A.util.removeClass(forclose, 'slds-is-open');
        
        var lookUpTarget = component.find("lookupField");
        $A.util.addClass(lookUpTarget, 'slds-hide');
        $A.util.removeClass(lookUpTarget, 'slds-show');  
    },
    getKPIDetails : function(component,event,helper){
        var project=component.get('v.selectedRecord');
        if(project.Id != null || project.Id != undefined){
            
            helper.getKPIDetailsHelper(component,event,helper); 
            helper.getDetails(component,event,helper);
        }
        else{
            alert('please select project');
        }
        
    },
    handleSelect : function (component, event, helper) {
        var stepName = event.getParam("detail").value;
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": "Toast from " + stepName
        });
        toastEvent.fire();
    },
    toggleStep4: function (cmp) {
        cmp.set('v.showStep4', !cmp.get('v.showStep4'));
    }
    ,
    openModel: function(component, event, helper) {
        // for Display Model,set the "isOpen" attribute to "true"
        //alert(event.getSource().get("v.title"));
        //helper.getDetails(component,event,helper);
        component.set("v.isOpen", true);
        var result = component.get('v.showDetailsMap');
        component.set('v.KPIParameter',event.getSource().get("v.title"));
        component.set('v.showDetails',result[event.getSource().get("v.title")]);
        for(var key in result) {
            if(result.hasOwnProperty(key)) {
                var value = result[key];
                console.log(value);
                console.log(key);
                //do something with value;
            }
        }
        
    },
    
    closeModel: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.isOpen", false);
    },
    
    likenClose: function(component, event, helper) {
        // Display alert message on the click on the "Like and Close" button from Model Footer 
        // and set set the "isOpen" attribute to "False for close the model Box.
        alert('thanks for like Us :)');
        component.set("v.isOpen", false);
    },
})