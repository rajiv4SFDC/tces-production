trigger EventTrigger on Event (after insert,after update,after delete) {
    
    if(trigger.isInsert)
    {
        EventTriggerHandler.afterInsertHandler(trigger.new);
    }
    if(trigger.isUpdate)
    {
        EventTriggerHandler.afterUpdateHandler(trigger.new, trigger.oldmap);
    }
    if(trigger.isDelete)
    {
        EventTriggerHandler.afterDeleteHandler(trigger.old);
    }

}