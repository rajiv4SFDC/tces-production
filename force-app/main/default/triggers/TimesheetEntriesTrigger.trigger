/*
Created By: Dheeraj Gangulli.
Created Date: 15/06/2016.
Last Modified By: Dheeraj Gangulli.
Last Modified Date: 15/06/2016.
Description: This trigger is used to map the project and resource Ids which are coming from Zoho reports.
*/
trigger TimesheetEntriesTrigger on Timesheet__c (before insert, before update) {
    
    Map<String, Id> projectIdMap = new Map<String, Id>(); //to store the project id(coming from zoho) and salesforce Id
    Map<String, Id> resourceIdMap = new Map<String, Id>(); //to store the resource id(coming from zoho) and salesforce Id
    Map<Id, Salary__c> resourceSalaryMap = new Map<Id, Salary__c>();
    list<Salary__C> resourceSalary = new list<salary__C>();
    
    //Constructing Map here
    for(Project_Profit__c p : [SELECT Id, Project_Id__c FROM Project_Profit__c]){
        projectIdMap.put(p.Project_Id__c, p.Id);
    }
    
    for(Resource_Profit__c r : [SELECT Id, Resource_Id__c FROM Resource_Profit__c]){
        resourceIdMap.put(r.Resource_Id__c, r.Id);
    }
    System.debug('Resource Map ------------>'+resourceIdMap);
    
    for(Salary__c s : [SELECT Id, Start_Date__c, End_Date__c, Cost_Working_Hour__c, Resource_Name__c FROM Salary__c ORDER BY CreatedDate ASC]){
        resourceSalaryMap.put(s.Resource_Name__c, s);
        resourceSalary.add(s);
        system.debug('==============='+s);
    }
    System.debug('Salary Map--------------->'+resourceSalaryMap);
    
    if(trigger.isInsert && trigger.isBefore){
        for(Timesheet__c t : trigger.New){   
            if(!projectIdMap.isEmpty() && projectIdMap.containsKey(t.Project_ID__c) && projectIdMap.get(t.Project_ID__c) != null){
                t.Project_Name__c = projectIdMap.get(t.Project_ID__c);
            }     
            
            if(!resourceIdMap.isEmpty() && resourceIdMap.containsKey(t.Owner_ID__c) && resourceIdMap.get(t.Owner_ID__c) != null){
                t.Resource_Name__c = resourceIdMap.get(t.Owner_ID__c);
            }   
            
            //checking the dates
           /* if(!resourceSalaryMap.isEmpty() &&  resourceSalaryMap.containsKey(resourceIdMap.get(t.Owner_ID__c)) && resourceSalaryMap.get(resourceIdMap.get(t.Owner_ID__c)) != null){
                if(resourceSalaryMap.get(resourceIdMap.get(t.Owner_ID__c)).Start_Date__c <= t.Date__c && resourceSalaryMap.get(resourceIdMap.get(t.Owner_ID__c)).End_Date__c >= t.Date__c){
                    t.Cost_Working_Hour__c = resourceSalaryMap.get(resourceIdMap.get(t.Owner_ID__c)).Cost_Working_Hour__c;
                }
            }*/
            for(Salary__C s : resourceSalary){
                system.debug('inside for loop'+s.Resource_Name__c+'------------'+resourceIdMap.get(t.Owner_ID__c));
                if(s.Resource_Name__c == resourceIdMap.get(t.Owner_ID__c)){
                    system.debug('inside first  if');
                    if(s.Start_Date__c <= t.Date__c && s.End_Date__c >= t.Date__c){
                        t.Cost_Working_Hour__c = s.Cost_Working_Hour__c;
                        system.debug('inside second if');
                    }
                }
            }
        } //end of trigger loop.
    }//end of trigger isInsert and isBefore
    
    if(trigger.isUpdate &&trigger.isBefore){
        for(Timesheet__c t : trigger.New){        
            //t.Project_Name__c = projectIdMap.get(t.Project_ID__c);
            //t.Resource_Name__c = resourceIdMap.get(t.Owner_ID__c);
            
            for(Salary__C s : resourceSalary){
                system.debug('inside for loop');
                if(s.Resource_Name__c == resourceIdMap.get(t.Owner_ID__c)){
                    system.debug('inside first  if');
                    if(s.Start_Date__c <= t.Date__c && s.End_Date__c >= t.Date__c){
                        t.Cost_Working_Hour__c = s.Cost_Working_Hour__c;
                        system.debug('inside second if');
                    }
                }
            }
            
            //checking the dates
            /*if(!resourceSalaryMap.isEmpty() &&  resourceSalaryMap.containsKey(resourceIdMap.get(t.Owner_ID__c)) && resourceSalaryMap.get(resourceIdMap.get(t.Owner_ID__c)) != null){
                if(resourceSalaryMap.get(resourceIdMap.get(t.Owner_ID__c)).Start_Date__c <= t.Date__c && resourceSalaryMap.get(resourceIdMap.get(t.Owner_ID__c)).End_Date__c >= t.Date__c){
                    t.Cost_Working_Hour__c = resourceSalaryMap.get(resourceIdMap.get(t.Owner_ID__c)).Cost_Working_Hour__c;
                }
            }*/
        } //end of trigger loop.
    }
    
}