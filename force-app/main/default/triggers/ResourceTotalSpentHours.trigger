/**
 * @Author      :       Deepa Deshpande
 * @Date        :       11/27/2018
 * @Desc        :      Trigger for totalhoursspent of inidividual user against requirement
 * */
trigger ResourceTotalSpentHours on Milestone1_Task__c (after insert,after update,after delete,after undelete) {
    
 set<ID> AccIds = new set<ID>();
    Set<id> projids = new Set<id>();
    
    if(trigger.isinsert || trigger.isundelete){
        for(Milestone1_Task__c opp : trigger.new){
            AccIds.add(opp.Assigned_To__c);
        }
    }
    if(trigger.isdelete){
        for(Milestone1_Task__c opp : trigger.old){
            AccIds.add(opp.Assigned_To__c);           
        }        
    }
    
    if(trigger.isupdate){
        for(Milestone1_Task__c opp:trigger.new){
            System.debug('Fired update');
            AccIds.add(opp.Assigned_To__c);
           
            
            if(trigger.oldmap.get(opp.id).Assigned_To__c != opp.Assigned_To__c && trigger.oldmap.get(opp.id).Assigned_To__c!= null ){
                AccIds.add(trigger.oldmap.get(opp.id).Assigned_To__c);
            }            
        }
       
    map<id,double> amtmap = new map<id,double>();
    List<Milestone1_Task__c> tskmap = [Select id,Project_Milestone__r.Project__c from Milestone1_Task__c where id in:Trigger.new];
    for(Milestone1_Task__c mtsk : tskmap)
    {
        projids.add(mtsk.Project_Milestone__r.Project__c);
    }
    System.debug('projids-->'+projids);
    List<aggregateresult> aglist = [select Assigned_To__c,SUM(Total_Hours_Consumed__c) SOA,count(id) cc from Milestone1_Task__c where Assigned_To__c in:AccIds and Project_Milestone__r.Project__c in: projids group by Assigned_To__c ];
    System.debug('aglist--------->'+aglist);
    
    for(aggregateresult ag : [select Assigned_To__c,SUM(Total_Hours_Consumed__c) SOA,count(id) cc from Milestone1_Task__c where Assigned_To__c in:AccIds and Project_Milestone__r.Project__c in: projids group by Assigned_To__c ]){
        amtmap.put((ID)ag.get('Assigned_To__c'), double.valueof(ag.get('SOA')));
        
    }
    list<Line_Items__c> acclist = [select Id, User__c,Total__c, Project__c from Line_Items__c where User__c IN: AccIds AND Project__c IN:projids];
    system.debug('*****acclist*****' +acclist);
    for(Line_Items__c usr : acclist){
        
        if(amtmap.containskey(usr.User__c)){
            system.debug('Total hrs'+amtmap.get(usr.User__c));
            usr.Total__c= amtmap.get(usr.User__c);
            system.debug('acnt'+usr.Total__c);
        }else{
            usr.Total__c= 0;
        } 
    }
    
    if(acclist.size()>0){
        system.debug('AccList'+acclist);
        update acclist;
    }
    }
    
}