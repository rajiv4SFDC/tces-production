trigger resourceTrigger on Line_Items__c (after Insert) {
    if(trigger.isInsert && trigger.isAfter){
        resourceHandler.shareResource(trigger.new);
    }
}