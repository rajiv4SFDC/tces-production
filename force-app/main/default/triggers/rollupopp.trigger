trigger rollupopp on Milestone1_Task__c(after delete,after update,after insert,after undelete) {
    
    set<ID>AccIds = new set<ID>();   
    string uid = userinfo.getUserId();
    if(trigger.isinsert || trigger.isundelete){
        for(Milestone1_Task__c opp : trigger.new){
            AccIds.add(opp.Assigned_To__c);
        }
    }
    if(trigger.isdelete){
        for(Milestone1_Task__c opp : trigger.old){
            AccIds.add(opp.Assigned_To__c);           
        }        
    }
    
    if(trigger.isupdate){
        for(Milestone1_Task__c opp:trigger.new){
            AccIds.add(opp.Assigned_To__c);
            if(trigger.oldmap.get(opp.id).Assigned_To__c != opp.Assigned_To__c && trigger.oldmap.get(opp.id).Assigned_To__c!= null ){
                AccIds.add(trigger.oldmap.get(opp.id).Assigned_To__c);
            }            
        }
    }    
    map<id,double> amtmap = new map<id,double>();
    for(aggregateresult ag : [select Assigned_To__c,SUM(Total_Hours_Consumed__c) SOA,count(id) cc from Milestone1_Task__c where Assigned_To__c in:AccIds group by Assigned_To__c ]){
        amtmap.put((ID)ag.get('Assigned_To__c'), double.valueof(ag.get('SOA')));
        
    }
    list<User> acclist = [select Id, Total_Hours_Spent__c, Total__c from USER where Id IN: AccIds LIMIT 1];
    
    for(User usr : acclist){
        
        if(amtmap.containskey(usr.Id)){
            system.debug('Total hrs'+amtmap.get(usr.Id));
            usr.Total_Hours_Spent__c= amtmap.get(usr.Id);
            usr.Total__c  = amtmap.get(usr.Id);//Added because Total Hrs spent is used else where and is not getting calculated properly
            system.debug('acnt'+usr.Total_Hours_Spent__c);
        }else{
            usr.Total_Hours_Spent__c= 0;
        } 
    }
    
    if(acclist.size()>0){
        system.debug('AccList'+acclist);
        update acclist;
    }
    
}