trigger CreateTask on Milestone1_Milestone__c (after insert) 
{
   if(Trigger.isAfter && Trigger.isInsert)
   {
      Milestone1_Milestone_Task_Creation.MilestoneTasks(trigger.new);
      Milestone1_Milestone_Task_Creation.doHandler();        
   }
}