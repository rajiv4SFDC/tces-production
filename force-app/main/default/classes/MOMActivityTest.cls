@isTest
public class MOMActivityTest {
    static testMethod void pdf(){
        Account acc=new Account();
        acc.Name='KVP Testing Account';
        insert acc;
        
         Contact con=new Contact();
        con.LastName='Test Contact';
        con.AccountId=acc.id;
        con.Email='xyz@gmail.com';
        insert con;
        
        Opportunity__c opp=new Opportunity__c();
        opp.Name='KVP Testing Account Opportunity';
        opp.Account__c = acc.Id;
        opp.Opportunity_Category__c='New Customer - New Opportunity';
        opp.Type__c='Services';
        opp.Close_Date__c=System.today();
        opp.First_Point_of_Escalation_Contact_Email__c=con.Id;//remove b4 prodctn
        opp.Second_Point_of_Escalation_Contact_Email__c=con.Id;
        insert opp;
        
        system.debug('op'+opp);
        Milestone1_Project__c project=new Milestone1_Project__c();
        project.Name='KVP Test Class Project';
        project.Opportunity__c=opp.Id;
        project.Milestone_Frequency__c='Month';
        project.Account__c=acc.Id;//remove b4 prodctn
        project.Total_Hours_Estimate__c=450;
        project.Risks_In_The_Project__c='Low';
        project.People_Risks_In_The_Project__c='Low';
        project.Project_Complexity__c='Low';
        project.Actual_Start_Date__c=System.today();
        project.Actual_End_Date__c=System.today();
        insert project;
         system.debug('proj'+project);
        
        Meeting_Notes__c meet=new Meeting_Notes__c();
        meet.Name='Meeting Name';
        meet.Project__c=project.Id;
        meet.Meeting_Place__c='London';
        meet.Start_Time__c=System.now();
        meet.End_Time__c=System.now();
        meet.Dicisions__c='abc testing abc';
       insert meet;
        
        Task tsk=new Task();
        tsk.Subject='Testing Task';
        tsk.Status='Deferred';
        tsk.OwnerId=UserInfo.getUserId();
        tsk.whatId=project.Id;
        insert tsk;
          
         Test.startTest();
        Test.setCurrentPageReference(new PageReference('Page.MOMTemplateOpportunity')); 
        System.currentPageReference().getParameters().put('id', project.Id);
        ApexPages.StandardController sc = new ApexPages.standardController(project);
        
        MOMActivityController controller = new MOMActivityController(sc);
        
        Test.stopTest();
        
    }

}