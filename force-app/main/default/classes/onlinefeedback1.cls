/***********************************************************
* Created By:   Ankit Kumar(KVP Business Solutions)
* Created Date: 12/June/2018
* Modified By : 
* Description : This is to insert the ANNONYMOUS FEEDBACK from site. 
************************************************************/
public class onlinefeedback1{ 
  public Integer feedback{get;set;}
  public Feedback1__c feed{get;set;}
  //public string selectedValue {get;set;}
  public string radioHidden {get;set;}
    public onlinefeedback1(){
        feedback = 0;
        Id RecordTypeId   = Schema.SObjectType.Feedback1__c.getRecordTypeInfosByName().get('Anonymous Feedback').getRecordTypeId();    
        feed              = new Feedback1__c();
        feed.RecordTypeId = RecordTypeId;
        system.debug('-------radioHidden -------'+radioHidden  );
    }
    public PageReference doSave(){    
        feed.rating__c =string.valueof(feedback);
        feed.Category__c=radioHidden ; 
        Insert feed;     
        PageReference ref= new PageReference('/apex/Thanksfeedback');
        return ref;
    }  
    /*public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('Work Environment','Work Environment')); 
        options.add(new SelectOption('HR Policies','HR Policies')); 
        options.add(new SelectOption('Learning and Development','Learning and Development')); 
        options.add(new SelectOption('Management','Management')); 
        return options; 
    }  */     
}