@isTest
public class CandidatePageControllerTest{
    public static testMethod void CandidatePageControllerTestmethod(){
        Id RecordTypeId = Schema.SObjectType.Candidate__c.getRecordTypeInfosByName().get('Online Candidate').getRecordTypeId();              
        Candidate__c objCan = new Candidate__c();        
        objCan.Current_Location__c='Goa';
        objCan.Applying_for_the_Position__c='CEO';
        objCan.Email_ID__c='test@gmail.com';
        objCan.Mobile_No__c='8654789654';
        objCan.Name='Test';
        objCan.RecordTypeId= RecordTypeId;
        insert objCan;
        
        String str='Testing';
        Attachment attach= new Attachment();
        attach.Body=Blob.valueOf(str);
        attach.Name='Test';
        attach.IsPrivate=True;
        attach.ParentId=objCan.Id;        
        Insert attach;
                 
        test.startTest();
        CandidatePageController can = new CandidatePageController();  
        can.doSaveData();          
        test.stopTest();
     }  
}