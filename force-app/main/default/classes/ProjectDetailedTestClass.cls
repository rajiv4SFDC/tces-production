@isTest
public class ProjectDetailedTestClass {
    static testMethod void pdf(){
        
        Milestone1_Task__c c1;
        Account acc=new Account();
        acc.Name='KVP Testing Account';
        insert acc;
        
        //remove before moving in production
         Contact con=new Contact();
        con.LastName='Test Contact';
        con.AccountId=acc.id;
        con.Email='xyz@gmail.com';
        insert con;
        
        
        Opportunity__c opp=new Opportunity__c();
        opp.Name='KVP Testing Account Opportunity';
        opp.Account__c = acc.Id;
        opp.Opportunity_Category__c='New Customer - New Opportunity';
        opp.Type__c='Services';
        opp.Close_Date__c=System.today();
        //opp.First_Point_of_Escalation_Contact_Email__c=con.Id;
        //opp.Second_Point_of_Escalation_Contact_Email__c=con.Id;
        insert opp;
        
        system.debug('op'+opp);
        Milestone1_Project__c project=new Milestone1_Project__c();
        project.Name='KVP Test Class Project';
        project.Opportunity__c=opp.Id;
        project.Account__c=acc.Id;
        project.Milestone_Frequency__c='Month';
        project.Total_Hours_Estimate__c=450;
        project.Risks_In_The_Project__c='Low';
        project.People_Risks_In_The_Project__c='Low';
        project.Project_Complexity__c='Low';
        project.Actual_Start_Date__c=System.today();
        project.Actual_End_Date__c=System.today();
        insert project;
        
        Id RecordTypeIdModule = Schema.SObjectType.Milestone1_Project__c.getRecordTypeInfosByName().get('Project Module').getRecordTypeId();
        Milestone1_Project__c proModule=new Milestone1_Project__c();
        proModule.Name='KVP Test Class Project Module 1';
        proModule.Account__c=acc.Id;
        proModule.Project__c=project.Id;
        proModule.Opportunity__c=opp.Id;
        proModule.RecordTypeId=RecordTypeIdModule;
        proModule.Risk_mitigation_plan__c='xyz';
        insert proModule;
        
        Milestone1_Milestone__c	miles=new Milestone1_Milestone__c();
        miles.Name='KVP Milestone Test Class';
        miles.Project__c = project.id;
        miles.Focus_of_this_milestone__c='aa';
        miles.Kickoff__c=System.today();
        miles.Deadline__c=System.today();
        insert miles;
        
        Id RecordTypeIdAPlan = Schema.SObjectType.Milestone1_Task__c.getRecordTypeInfosByName().get('A-Planned').getRecordTypeId();
        Milestone1_Task__c task=new Milestone1_Task__c();
        task.Name='A-Planned Task';
        task.Assigned_To__c=UserInfo.getUserId();
        task.Project_Milestone__c=miles.Id;
        task.Description__c=' abc abc abc testing 123';
        task.Start_Date1__c=System.today();
        task.Due_Date1__c=System.today();
        task.Project_Module__c=proModule.Id;
        task.RecordTypeId=RecordTypeIdAPlan;
        insert task;
        
        Id RecordTypeIdTest = Schema.SObjectType.Milestone1_Task__c.getRecordTypeInfosByName().get('C-Test Case').getRecordTypeId();
        Milestone1_Task__c req=new Milestone1_Task__c();
        req.Name='KVP Test UAT';
        req.Project_Milestone__c=miles.Id;
        req.Predecessor_Task__c=task.Id;
        req.Assigned_To__c=UserInfo.getUserId();
        req.Test_Case_Type__c='Positive';
        req.RecordTypeId=RecordTypeIdTest;
        req.UAT_Test_Case__c=true;
        req.Test_Description__c='x';
        req.Test_Steps__c='a';
        req.Expected_Results__c='a';
        insert req;  
        system.debug('req'+req);
        
        User_Story1__c us=new User_Story1__c();
        us.Use_Case_Title__c = 'xyz testing';
        us.Pre_Conditions__c='pre conditions';
        us.Process_Steps__c='proces steps';
        us.Use_Case_Documentation__c='use case documentation';
        us.Project_Acceptance_Criteria__c='xyz';
        us.Assumption_and_Risks__c='No risks';
        us.Actors__c='No actors';
        us.Project__c=project.Id;
        insert us;
        
        Test.startTest();
        Test.setCurrentPageReference(new PageReference('Page.ProjectDetailedReportF')); 
        System.currentPageReference().getParameters().put('id', project.Id);
        ApexPages.StandardController sc = new ApexPages.standardController(project);
        
        ProjectDetailedControllerPDF controller = new ProjectDetailedControllerPDF(sc);
        
        Test.stopTest();
        
        
    }
}