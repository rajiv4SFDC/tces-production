/******************************************************************************
Created by: Vijay Kumar H R
Created date: 22/02/2019
Class Name : KPITestClass
Description : test Class for KPI customLookupController Class.
***********************************************************************************/
@isTest
public class KPITestClass {
    @isTest static void kpiTest() {
        //Creating Two Users testu1 and testu2.
        list<User>userList=new list<User>();
        User testu1 = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Developer Profile'].Id,
            LastName = 'testUser1',
            Email = 'testUser1@gmail.com',
            Username = 'testUser1@gmail11.com', //System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias1',
            TimeZoneSidKey = 'America/Los_Angeles',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'UTF-8'
        );
        User testu2 = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Developer Profile'].Id,
            LastName = 'testUser2',
            Email = 'testUser2@gmail.com',
            Username = 'testUser21@gmail.com',
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias2',
            TimeZoneSidKey = 'America/Los_Angeles',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            EmailEncodingKey = 'UTF-8'
        );
        userList.add(testu1);
        userlist.add(testu2);
        insert userList;
        
        date d1=Date.today();
        date d2=d1.addDays(10);
        //Creating Account acc.
        Account acc=new Account();
        acc.Name='Test Account';
        acc.CurrencyIsoCode='INR';
        acc.Type='Customer';
        
        
        try{
            Insert acc;
        }
        catch(Exception e){
            system.debug(e);
        }
        //Creating Opportunity Opp1 and Opp2.
        list<Opportunity__c> oppList=new list<Opportunity__c>();
        Opportunity__c opp1=new Opportunity__c();
        opp1.Account__c=acc.Id;
        opp1.Name='Test Class Opp1';
        opp1.Opportunity_Category__c = 'New Customer - New Opportunity';
        opp1.Type__c='Services';
        opp1.Close_Date__c=d2;
        
        oppList.add(opp1);
        
        Opportunity__c opp2=new Opportunity__c();
        opp2.Account__c=acc.Id;
        opp2.Name='Test Class Opp2';
        opp2.Opportunity_Category__c = 'New Customer - New Opportunity';
        opp2.Type__c='Services';
        opp2.Close_Date__c=d2;
        oppList.add(opp2);
        insert oppList;
        
        //Creating Projects Project1 and Project2. 
        list<Milestone1_Project__c> projects=new list<Milestone1_Project__c>();
        Milestone1_Project__c project1=new 	Milestone1_Project__c();
        project1.Name='testProject1';
        project1.Actual_Start_Date__c=d1;
        project1.Actual_End_Date__c=d2;
        project1.Status__c='Active';
        project1.Milestone_Frequency__c='Week';
        project1.Risks_In_The_Project__c='Low';
        project1.Project_Complexity__c='Low';
        project1.People_Risks_In_The_Project__c='Low';
        project1.Opportunity__c=opp2.Id;
        project1.Kickoff__c=d1;
        project1.Deadline__c=d2;
        project1.Account__c=acc.id;
        insert project1;
        
        Invoice_Profit__c invoice = new Invoice_Profit__c();
        invoice.Amount__c = 100;
        invoice.CurrencyIsoCode = 'INR';
        invoice.Invoice_Number__c = '12345';
        invoice.Date__c = d2;
        insert invoice;
        
        Invoice_Profit__c invoice1 = new Invoice_Profit__c();
        invoice1.Amount__c = 100;
        invoice1.CurrencyIsoCode = 'INR';
        invoice1.Invoice_Number__c = '12345';
        invoice1.Date__c = d1.addDays(-5);
        insert invoice1; 
        
        Payment_Schedule__C ps = new Payment_Schedule__c();
        ps.Sales_Invoice_Amount__c = 0;
        ps.Sales_Invoice_Date__c = d1;
        ps.Invoice__c = invoice.Id;
        ps.File_Attached__c = true;
        ps.Approved__c = true;
       // ps.Invoice_date_F__c = d2;
        ps.Opportunity__c = opp2.id;
        ps.Project__c = project1.Id;
        ps.Type__c = 'Services';
        insert ps;
         Payment_Schedule__C ps1 = new Payment_Schedule__c();
        ps1.Sales_Invoice_Amount__c = 0;
        ps1.Sales_Invoice_Date__c = d2;
        ps1.File_Attached__c = true;
        ps1.Approved__c = true;
        ps1.Invoice__c = invoice1.Id;
        //ps1.Invoice_date_F__c = d1;
        ps1.Opportunity__c = opp2.id;
        ps1.Project__c = project1.Id;
        ps1.Type__c = 'Services';
        insert ps1;
     customLookUpController.fetchLookUpValues('test','Milestone1_Project__c');
        customLookUpController.fetchUserQueueValues('testUser1');
        customLookUpController.getCSATValues(project1.id);
        customLookUpController.getCalculatedData();
       
       
    }
}