@isTest(seeAllData = TRUE)

public class testResourceHandler{
    public static testMethod void method1(){
        User u = [Select Id from User WHERE Profile.Name LIKE '%Admin%' and isActive = TRUE LIMIT 1];
        
        Opportunity__c opp = new Opportunity__c();
        opp.Name = 'cx';
        insert opp;
        
        Milestone1_Project__c pro = new Milestone1_Project__c();
        pro.Name = 'ABC';
        pro.Opportunity__c = opp.Id;
        insert pro;
        
        Line_Items__c li = new Line_Items__c();
        li.Opportunity__c = opp.Id;
        li.User__c = u.Id;
        li.Project__c = pro.Id;
        insert li;
    }
}