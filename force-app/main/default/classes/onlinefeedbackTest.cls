@isTest
public class onlinefeedbackTest{
    public static testMethod void onlinefeedbackTestmethod(){
        Id RecordTypeId   = Schema.SObjectType.Feedback1__c.getRecordTypeInfosByName().get('Anonymous Feedback').getRecordTypeId();              
        Feedback1__c objFeed = new Feedback1__c();        
        objFeed.rating__c='2';
        objFeed.Category__c='Management';
        objFeed.RecordTypeId= RecordTypeId;
        insert objFeed;
        
        test.startTest();
        onlinefeedback1 feed = new onlinefeedback1();  
        feed.doSave(); 
        test.stopTest();
     }  
}