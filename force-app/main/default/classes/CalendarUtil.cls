/*
    Developed By : KVP Business Solutions
*/
public class CalendarUtil {
    private List<Week> weeks; 
    public Date firstDate; // always the first of the month
    private Date upperLeft; 
    public Boolean DayView = false;
    public CalendarUtil( Date value ) {
        weeks = new List<Week>();
        firstDate = value.toStartOfMonth();
        upperLeft = firstDate.toStartOfWeek();
        Date tmp = upperLeft;
        for (Integer i = 0; i < 5; i++) {
            Week w = new Week(i+1,tmp,value.month()); 
            system.assert(w!=null); 
            this.weeks.add( w );
            tmp = tmp.addDays(7);
        }
    }
    public List<Date> getValidDateRange() { 
       List<Date> ret = new List<Date>();
        ret.add(upperLeft);
        ret.add(upperLeft.addDays(5*7) );
        return ret;
    }
    public String getMonthName() { 
        return DateTime.newInstance(firstDate.year(),firstdate.month(),firstdate.day()).format('MMMM');
    } 
    public String getYearName() {
        return DateTime.newInstance(
        firstDate.year(),firstdate.month(),firstdate.day()).format('yyyy');
    } 
    public String[] getWeekdayNames() { 
        Date today = system.today().toStartOfWeek();
        DateTime dt = DateTime.newInstanceGmt(today.year(),today.month(),today.day());  
        list<String> ret = new list<String>();
        for(Integer i = 0; i < 7;i++) { 
            ret.add( dt.formatgmt('EEEE') );
            dt= dt.addDays(1);
        } 
        return ret;
    }
    public Date getfirstDate() { return firstDate; } 
    public List<Week> getWeeks() { 
      return this.weeks; 
    }
    public List<Week> getCurrentWeek(Date d){
        List<Week> returnWeek = new List<Week>();
        for(Week w:weeks){
            for(Day c: w.getDays()){
                if( d.isSameDay(c.theDate)){
                    returnWeek.add(w);
                    break;
                }
            }
        }
        weeks = returnWeek;
        return returnWeek;
    }
/**********helper classes to define a month in terms of week and day************/
    public  with sharing class Week {
        public List<Day> days;
        public Integer weekNumber; 
        public Date startingDate; // the date that the first of this week is on
        public List<Day> getDays() { return this.days; }
        public Week () { 
            days = new List<Day>();  
        }
        public Week(Integer value,Date sunday,Integer month) { 
            this();
            weekNumber = value;
            startingDate = Sunday;
            Date tmp = startingDate;
            for (Integer i = 0; i < 7; i++) {
                Day d = new Day( tmp,month ); 
                tmp = tmp.addDays(1);
                d.dayOfWeek = i+1;    
                days.add(d);
            } 
        }
       // public Integer getWeekNumber() { return this.weekNumber;}
       // public Date getStartingDate() { return this.startingDate;}
    }
    public with sharing class Day {
    
     public Integer getDayOfYear() { return theDate.dayOfYear(); }
     public Integer getMonth(){return theDate.month();}
        public String getFormatedDate() { return formatedDate; }
        public Integer getDayNumber() { return dayOfWeek; }
        public String getCSSName() {  return cssclass; }
        public Date theDate;
        public Integer month, dayOfWeek;
        public String formatedDate; // for the formated time  
        private String cssclass = 'calnActive';
        public Date getDate() { return theDate; }
        public Integer getDayOfMonth() { return theDate.day(); }
//public Integer getDayOfMon() {return theDate.month();}
       /* public String getDayOfMonth2() { 
            if ( theDate.day() <= 9 ) 
            return '0'+theDate.day(); 
            return String.valueof( theDate.day()); 
        }*/
        public Day(Date value,Integer vmonth) { 
            theDate=value; month=vmonth;   
            formatedDate = '12 21 08';// time range..
            //9:00 AM - 1:00 PM
            if ( theDate.daysBetween(System.today()) == 0 )cssclass ='calnToday';
            if ( theDate.month() != month)cssclass = 'calnInactive';
        }

    }
    
   
}