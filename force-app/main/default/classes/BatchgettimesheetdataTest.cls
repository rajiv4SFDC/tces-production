@isTest
public class BatchgettimesheetdataTest
{
    public static testmethod void testm()
    {
        // Create records in  Project_Profit__c object
        
        // Create records in  Project_Profit__c object
        Date d = system.today();
        list< Project_Profit__c> ProjectProfits = new list< Project_Profit__c>();
        ProjectProfits.add(new Project_Profit__c(Name='ProjectProfit1',Component_Type__c='task',Project_Id__c='x',Timesheet_Date__c=d));
        ProjectProfits.add(new Project_Profit__c(Name='ProjectProfit1',Component_Type__c='bug',Project_Id__c='y',Timesheet_Date__c=d));
        
        insert ProjectProfits;
        
        // Create records in Resource_Profit__c object
        
        list<Resource_Profit__c> ResourceProfits = new list<Resource_Profit__c>();
        ResourceProfits.add(new Resource_Profit__c(Resource_Id__c='123',Name='Res1'));
        ResourceProfits.add(new Resource_Profit__c(Resource_Id__c='456',Name='Res2'));
        insert ResourceProfits;
        
        // Call batch class
        
        Test.startTest();
         Batchgettimesheetdata b = new Batchgettimesheetdata();
         Database.executeBatch(b);
        // RestRequest req = new RestRequest(); 
         //RestResponse res = new RestResponse();
         //req.requestURI = 'https://projectsapi.zoho.com/restapi/portal/kvpbusiness/projects';  //Request URL
         //req.httpMethod = 'GET';//HTTP Request Type
         //req.requestBody = Blob.valueof(JsonMsg);
         //RestContext.request = req;
         //RestContext.response= res;
        //String reqBody = res.responseBody.toString();
        //Batchgettimesheetdata btd = new Batchgettimesheetdata();
        //btd.start(null);
        //btd.execute(null,ProjectProfits);
        //btd.finish(null);
        Test.stopTest();    
    
        
    }
}