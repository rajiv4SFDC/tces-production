/**************************************************************************************************************************************************************************************
* Company:      KVP Business Solutions Pvt. Ltd
* Created By:   Subhajit Ghosh
* Created Date: 07.02.2019 
* Description:  This is a apex class that generates PDF for the Milestone object along with 
requirements and their test cases. It is the controller to "MilestoneDetailedReport" VF Page  
*******************************************************************************************************************************************************************************************/

public class MilestoneDetailedController {
    public List<Milestone1_Task__c> reqWithTestCaseList{set;get;}
    public List<Milestone1_Task__c> reqWithoutTestCaseList{set;get;}
    public List<Milestone1_Task__c> testCaseWithoutReqList{set;get;}
    public List<Line_Items__c> projectOwnerDevList{set;get;}
    public List<Milestone1_Milestone__c> projectIdList{set;get;}
    public List<Milestone1_Project__c> ProjectOwnerInfo{set;get;}
    public string ProjectId;
    Set<Id> reqId;
    
    public MilestoneDetailedController(ApexPages.StandardController stdController){
        reqWithTestCaseList=new List<Milestone1_Task__c>();
        reqWithoutTestCaseList=new List<Milestone1_Task__c>();
        testCaseWithoutReqList=new List<Milestone1_Task__c>();
        projectIdList=new List<Milestone1_Milestone__c>();
        ProjectOwnerInfo=new List<Milestone1_Project__c>();
        projectOwnerDevList=new List<Line_Items__c>();
        reqId=new Set<Id>();
        reqWithTestCaseList=[SELECT Name,Start_Date1__c,Due_Date1__c,Task_Stage__c,Assigned_To__r.Name,Test_Case_Type__c,Test_Description__c,Test_Steps__c,Expected_Results__c,
                             Predecessor_Task__r.Name,Predecessor_Task__r.Project_Module__r.Name,Predecessor_Task__r.Assigned_To__r.Name,Predecessor_Task__r.Risk_Escalation__c,Predecessor_Task__r.Description__c,
                             Predecessor_Task__r.Start_Date1__c,Predecessor_Task__r.Due_Date1__c,Predecessor_Task__r.Task_Stage__c,Predecessor_Task__r.Task_idea_source__c
                             FROM Milestone1_Task__c where Project_Milestone__c =: ApexPages.currentPage().getParameters().get('id')
                             AND Predecessor_Task__c!=NULL AND RecordType.Name =:'C-Test Case' AND (Predecessor_Task__r.RecordType.Name =: 'A-Planned' 
                                                                                                    OR Predecessor_Task__r.RecordType.Name =:'B-Ad-Hoc') ORDER BY Predecessor_Task__r.Start_Date1__c];
        
        for(Milestone1_Task__c miles:reqWithTestCaseList){
            reqId.add(miles.Predecessor_Task__r.Id);
        }
        
        reqWithoutTestCaseList=[SELECT Name,Start_Date1__c,Due_Date1__c,Task_Stage__c,Assigned_To__r.Name,RecordType.Name,Project_Module__r.Name,Task_idea_source__c,Description__c,Risk_Escalation__c
                                FROM Milestone1_Task__c where Project_Milestone__c =: ApexPages.currentPage().getParameters().get('id') AND id NOT IN :reqId
                                AND (RecordType.Name =:'A-Planned' OR RecordType.Name =:'B-Ad-Hoc') 
                                AND Name !='Daily Scrum Meeting' AND Name !='Customer Review Meeting' AND Name !='Internal Review Meeting' 
                                Order by Start_Date1__c];
        
     /*   testCaseWithoutReqList=[SELECT Name,Start_Date1__c,Due_Date1__c,Task_Stage__c,Assigned_To__r.Name,RecordType.Name
                                FROM Milestone1_Task__c where Project_Milestone__c =: ApexPages.currentPage().getParameters().get('id')
                                AND Predecessor_Task__c = NULL AND RecordType.Name =:'C-Test Case' ORDER BY Start_Date1__c];*/
        
        
        projectIdList=[SELECT project__c FROM Milestone1_Milestone__c WHERE id =:ApexPages.currentPage().getParameters().get('id')];
        System.debug('projectIdList:' +projectIdList);
        ProjectId=projectIdList[0].project__c;
        System.debug(ProjectId);
        
        projectOwnerDevList=[select id,User__r.name,Project__r.name,Project__r.Owner_Name__c,Role__c from Line_Items__c where Project__c =:ProjectId AND Role__c!='Project Manager'];
        System.debug('projectOwnerDevList '+projectOwnerDevList);
        
        ProjectOwnerInfo=[SELECT Owner_Name__c,Owner.Email,Owner.Phone,Name from Milestone1_Project__c WHERE id =:ProjectId];
        
        
        
    }
    
    
}