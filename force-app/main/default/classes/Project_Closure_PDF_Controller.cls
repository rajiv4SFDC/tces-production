/**************************************************************************************************************************************************************************************
* Company:      KVP Business Solutions Pvt. Ltd
* Created By:   Subhajit Ghosh
* Created Date: 05.02.2019 
* Description:  This is a apex class that generates a report for project closure providing all necessary details
 
*******************************************************************************************************************************************************************************************/

public class Project_Closure_PDF_Controller {
    
    
    public List<Milestone1_Task__c> uatRequirementList{set;get;}
    public List<Milestone1_Milestone__c> milestoneList{set;get;}
    public String projectId{set;get;}
    public List<Line_Items__c> teamInfo{set;get;}
    public List<Milestone1_Project__c> ownerInfo{set;get;}
    public Boolean uatCount{set;get;} 
    public Integer uatSize{set;get;}
    public List<Milestone1_Project__c> requirementDetailsList{set;get;}
    public List<Feedback1__c> feedbackList{set;get;}
    public List<Milestone1_Task__c> ideasByCustomerList{set;get;}
    public List<Milestone1_Task__c> ideasByKVPList{set;get;}
    public List<Payment_Schedule__c> paymentScheduleList{set;get;}
    public List<Contract__c> documentList{set;get;}
    public Decimal totalIdeasCount;
    public Decimal customerIdeasPercent{set;get;}
    public Decimal kvpIdeasPercent{set;get;}
    
    
    public Project_Closure_PDF_Controller(ApexPages.StandardController stdController){
        uatRequirementList=new  List<Milestone1_Task__c>();
        milestoneList=new List<Milestone1_Milestone__c>();
        teamInfo=new List<Line_Items__c>();
        uatCount=false;
        requirementDetailsList=new List<Milestone1_Project__c>();
        feedbackList=new List<Feedback1__c>();
        ideasByCustomerList=new List<Milestone1_Task__c>();
        ideasByKVPList=new List<Milestone1_Task__c>();
        paymentScheduleList=new List<Payment_Schedule__c>();
        documentList=new List<Contract__c>();
        
        milestoneList=[SELECT id,Name,Kickoff__c,Deadline__c,Total_Task_without_auto_created_task2__c,Total_Test_Cases__c,Total_Req_Executed__c,Total_Completed_Testcase_task__c	 from Milestone1_Milestone__c 
                       where Project__c =: ApexPages.currentPage().getParameters().get('id') AND Name != 'General Milestone'];
        System.debug('UAT milestoneList Case--->>'+milestoneList);
        
        
        uatRequirementList=[Select id from Milestone1_Task__c where Project_Milestone__c in: milestoneList AND UAT_Test_Case__c=TRUE];
        uatSize=uatRequirementList.size();
        if(uatRequirementList.size()>0){
            uatCount=true;
        }
        
        System.debug('UAT Test Case--->>'+uatRequirementList);
        
        ownerInfo=[SELECT Owner_Name__c,Owner.Email,Owner.Phone,Name FROM Milestone1_Project__c where id =: ApexPages.currentPage().getParameters().get('id')];
        teamInfo=[SELECT User__r.Name,Role__c FROM Line_Items__c where Project__c=:ApexPages.currentPage().getParameters().get('id') AND Role__c!='Project Manager'];
        
        
        feedbackList=[SELECT Project__c,Planned_Date_of_Feedback__c,Rate_us_on_the_Project_Progress__c,Rate_us_on_Quality_of_Work__c,Rate_us_on_our_understanding_of_your_biz__c,
                      Rate_us_on_Our_Team_Energy__c,Rate_us_of_Communication_Effectiveness__c,Contact__r.Name,Overall_Rating_Average__c  FROM Feedback1__c	
                      where Project__c =: ApexPages.currentPage().getParameters().get('id') AND RecordType.Name = 'Customer Monthly Feedback' ORDER BY Planned_Date_of_Feedback__c];
        
        System.debug('feedback Lists are-->>'+feedbackList);
        
        
        // requirementDetailsList=[SELECT Total_Requirements__c,Total_Requirements_Executed_Project__c,Rolled_up_Completion__c];      
        
        ideasByCustomerList=[SELECT id from Milestone1_Task__c where Project_Milestone__c in: milestoneList AND (RecordType.Name =:'A-Planned' OR RecordType.Name =:'B-Ad-Hoc') 
                             AND Name !='Daily Scrum Meeting' AND Name !='Customer Review Meeting' AND Name !='Internal Review Meeting'
                             AND Task_idea_source__c ='Customer shared with us']; 
        ideasByKVPList=[SELECT id from Milestone1_Task__c where Project_Milestone__c in: milestoneList AND (RecordType.Name =:'A-Planned' OR RecordType.Name =:'B-Ad-Hoc')
                        AND Name !='Daily Scrum Meeting' AND Name !='Customer Review Meeting' AND Name !='Internal Review Meeting' 
                        AND Task_idea_source__c ='KVP shared with customer'];  
        System.debug('ideasByCustomerList.size()'+ideasByCustomerList.size() );
        System.debug('ideasByKVPList.size()'+ideasByKVPList.size());
        totalIdeasCount=ideasByCustomerList.size() + ideasByKVPList.size();
        if(totalIdeasCount!=0){
            customerIdeasPercent= ((ideasByCustomerList.size()/totalIdeasCount)*100).setScale(2);
            kvpIdeasPercent=((ideasByKVPList.size()/totalIdeasCount)*100).setScale(2);
        }
        
        paymentScheduleList=[SELECT Invoice_Number__c,Invoice_Amount__c,Sales_Invoice_Date__c,Sales_Invoice_Amount__c,Operations_Invoice_Amount__c,Operations_Invoice_Date__c,Collected_Amount__c
                             FROM Payment_Schedule__c WHERE Project__c =: ApexPages.currentPage().getParameters().get('id')];
        documentList=[SELECT Document_Type__c,GDrive_Link__c,Status__c FROM Contract__c WHERE Project__c =: ApexPages.currentPage().getParameters().get('id')];
        
        
        
    }
}