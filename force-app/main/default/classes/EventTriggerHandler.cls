/***************************
 * Class Name: EventTriggerHandler
 * Created By: Rekha
 * Created Date: 07.04.2017
 * Purpose: To count no of Events on Opportunity object on insert,update and delete
 * *************************/

public class EventTriggerHandler {

    /* collects Ids after insert*/
    public static void afterInsertHandler(list<event> newList)
    {
        set<id> opportunityId = new set<id>();
        for(event eventObj:newList)
        {
           if(eventObj.WhatId !=NULL)
               opportunityId.add(eventObj.WhatId);
        }
        if(opportunityId.size() > 0)
        	updateEventCount(opportunityId);
    }
    
    /* collects Ids after update*/
    public static void afterUpdateHandler(list<event> newList,map<id,event> oldmap)
    {
        set<id> opportunityId = new set<id>();
        for(event eventObj:newList)
        {
            
            if(eventObj.WhatId!=oldmap.get(eventObj.id).WhatId)
            {
                if(eventObj.WhatId != null)
                	opportunityId.add(eventObj.WhatId);
                
                if(oldmap.get(eventObj.id).WhatId != null)
                	opportunityId.add(oldmap.get(eventObj.id).WhatId);
            }
            
        }
        if(opportunityId.size() > 0)
        	updateEventCount(opportunityId);
    }
    
    /* collects Ids after delete*/
    public static void afterDeleteHandler(list<event> oldList)
    {
        set<id> opportunityId = new set<id>();
        for(event eventObj:oldList)
        {
            if(eventObj.whatId != null)
            	opportunityId.add(eventObj.whatId);
        }
        if(opportunityId.size() > 0)
        	updateEventCount(opportunityId);

    }
    
    
    /*to update no of events on opportunity object*/
    public static void updateEventCount(set<id> opportunityIdset)
    {
        List<Opportunity__c> opportunityList = new List<Opportunity__c>();
        for(Opportunity__c opportunityObj : [SELECT id,Number_of_Activities__c,(SELECT id FROM events) FROM Opportunity__c WHERE id IN : opportunityIdset])
        {
            opportunityObj.Number_of_Events__c = opportunityObj.events.size();
            opportunityList.add(opportunityObj);
        }        
        try
        {
            if(!opportunityList.isEmpty())
            	update opportunityList;
        }
        catch(Exception e)
        {
            system.debug('Error'+e);
        }
    }
}