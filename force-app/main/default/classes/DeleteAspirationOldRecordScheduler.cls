/******************************************
 * Created By  : Ajeet Singh Shekhawat
 * Created On  : 18 Sep 2018
 * Modified By : Ajeet Singh Shekhawat
 * Modified On : 18 Sep 2018
 * Description : Scheduler class for DeleteAspirationOldRecordsBatch class.
				
******************************************/
global class DeleteAspirationOldRecordScheduler implements Schedulable {
	global void execute(SchedulableContext sc)
    {
        DeleteAspirationOldRecordsBatch asp = new DeleteAspirationOldRecordsBatch();
        Database.executeBatch(asp);
    }
}