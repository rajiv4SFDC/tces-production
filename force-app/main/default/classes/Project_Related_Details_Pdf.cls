/**************************************************************************************************************************************************************************************
 * Company:      KVP Business Solutions Pvt. Ltd
 * Created By:   Subhajit Ghosh
 * Created Date: 24.01.2019 
 * Description:  This is a apex class that generates PDF for the Project object along with 
                 List of UAT test cases. It is the controller to "Project_UAT_Cases" VF Page  
*******************************************************************************************************************************************************************************************/



public class Project_Related_Details_Pdf {
    public List<Milestone1_Task__c> uatRequirementList{set;get;}
    public List<Milestone1_Milestone__c> milestonePidList{set;get;}
    public String projectId{set;get;}
    public List<Line_Items__c> teamInfo{set;get;}
    public List<Milestone1_Project__c> ownerInfo{set;get;}
    public Boolean uatCount{set;get;} 
    public Integer uatSize{set;get;}
    
    
    public Project_Related_Details_Pdf(ApexPages.StandardController stdController){
        uatRequirementList=new  List<Milestone1_Task__c>();
        milestonePidList=new List<Milestone1_Milestone__c>();
        teamInfo=new List<Line_Items__c>();
        uatCount=false;
        
        milestonePidList=[SELECT id from Milestone1_Milestone__c where Project__c =: ApexPages.currentPage().getParameters().get('id')];
        System.debug('UAT milestonePidList Case--->>'+milestonePidList);
        
        
        uatRequirementList=[Select Name,Test_Case_Type__c from Milestone1_Task__c where Project_Milestone__c in: milestonePidList AND UAT_Test_Case__c=TRUE];
         uatSize=uatRequirementList.size();
        if(uatRequirementList.size()>0){
            uatCount=true;
            }
        
        System.debug('UAT Test Case--->>'+uatRequirementList);
        
        ownerInfo=[SELECT Owner_Name__c,Owner.Email,Owner.Phone,Name FROM Milestone1_Project__c where id =: ApexPages.currentPage().getParameters().get('id')];
        teamInfo=[SELECT User__r.Name,Role__c FROM Line_Items__c where Project__c=:ApexPages.currentPage().getParameters().get('id') AND Role__c!='Project Manager'];
        
        
        
    }

}