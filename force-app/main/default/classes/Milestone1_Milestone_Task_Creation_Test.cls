@isTest
private class Milestone1_Milestone_Task_Creation_Test
{

  static testmethod void setupTestData1()
  {
    test.startTest();
    Account AccList = new Account(Name='Test1');
    Insert AccList;
    
    Opportunity__c OppList = new Opportunity__c(Name='Test10', Account__c = AccList.Id, Opportunity_Category__c= 'New Customer - New Opportunity', Type__c='Services');
    Insert OppList;
    
    Milestone1_Project__c projList = new Milestone1_Project__c(Name='KVP Test', Opportunity__c=OppList.Id, Kickoff__c=System.today(), Deadline__c=System.today());
    Insert projList;
    
    Milestone1_Milestone__c milproj1 = new Milestone1_Milestone__c(Name='General Milestone', Kickoff__c=System.today(), Deadline__c=System.today(), Project__c = projList.Id);
    Insert milproj1;
      
    
    Milestone1_Milestone__c milproj2 = new Milestone1_Milestone__c(Name='Project Closure Documentation',  Kickoff__c=System.today(), Deadline__c=System.today(), Project__c = projList.Id);
    Insert milproj2;
    
    Milestone1_Milestone__c milproj3 = new Milestone1_Milestone__c(Name='Project Initiation Documentation', Kickoff__c=System.today(), Deadline__c=System.today(), Project__c = projList.Id);
    Insert milproj3;
    
    System.debug('mile id'+milproj1.id);
    if(milproj1.id !=null){  
    Milestone1_Task__c Task1 = new Milestone1_Task__c(Name = 'Name728', Project_Milestone__c = milproj1.id);
    Insert Task1;
      system.debug('@@@@@@@@' + Task1);
      }
    
    Milestone1_Task__c Task2 = new Milestone1_Task__c(Name = 'Name728', Project_Milestone__c = milproj2.id);
    Insert Task2;
    system.debug('#########' + Task2);
    
    Milestone1_Task__c Task3 = new Milestone1_Task__c(Name = 'Name728', Project_Milestone__c = milproj3.id);
    Insert Task3;
    system.debug('$$$$$$$$' + Task3);
    
    test.stopTest();
    
  }
  
}