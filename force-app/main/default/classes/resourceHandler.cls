public class resourceHandler{
    
    public static void shareResource(List<Line_Items__c> newList){
        List<Milestone1_Project__Share> shareList = new List<Milestone1_Project__Share>();
        for(Line_Items__c li : newList){
            if(li.Project__c != NULL){
                Milestone1_Project__Share pro = new Milestone1_Project__Share();
                pro.ParentId = li.Project__c;
                pro.UserOrGroupId = li.User__c;
                pro.AccessLevel = 'Edit';
                shareList.add(pro);
            }            
        }
        
        if(!shareList.isEmpty()){
            try{
                insert shareList;
            }catch(exception e){}
        }
    }
}