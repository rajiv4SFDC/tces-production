global class Batchgettimesheetdata implements Database.Batchable<sObject>, Database.AllowsCallouts{
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        
        String query = 'Select Id,Name,Component_Type__c,Project_Id__c,Timesheet_Date__c from Project_Profit__c';
        system.debug('query------------------->'+Database.getQueryLocator(query));
        return Database.getQueryLocator(query);
        
        
    }
    
    global void execute(Database.BatchableContext BC, List<Project_Profit__c> proj) {
        system.debug('proj---------------------------> '+proj);
        Project_Profit__c pp = [select id from Project_Profit__c limit 1];
        system.debug('ppppppppppppppp'+proj);
        //Variables to create date instance
        string mm;
        string dd;
        string year;
        String url;
        list<Timesheet__c> time_List = new list<Timesheet__c>();
        
        Map<String, Id> resourceIdMap = new Map<String, Id>(); //to store the resource id(coming from zoho) and salesforce Id
        Resource_Profit__c res = [SELECT Id, Resource_Id__c FROM Resource_Profit__c limit 1];
        
        system.debug('res------------------------------------->'+res);
        
        for(Resource_Profit__c r : [SELECT Id, Resource_Id__c FROM Resource_Profit__c]){
            resourceIdMap.put(r.Resource_Id__c, r.Id);
        }
        
        for(Project_Profit__c projdata : proj ){
            
            if(projdata.Timesheet_Date__c.month() < 10){
                mm = '0'+string.ValueOf(projdata.Timesheet_Date__c.month());
            }
            else{
                mm = string.ValueOf(projdata.Timesheet_Date__c.month()); 
            }       
            if(projdata.Timesheet_Date__c.day() < 10){
                dd = '0'+string.ValueOf(projdata.Timesheet_Date__c.day());
            } 
            else{
                dd = string.ValueOf(projdata.Timesheet_Date__c.day()); 
            }   
            year = string.valueOf(projdata.Timesheet_Date__c.year());
            //url which have project id and date dynamic
            url = 'https://projectsapi.zoho.com/restapi/portal/kvpbusiness/projects/'+projdata.Project_Id__c+'/logs/?AUTHTOKEN=de4b709bca5e79281838a1e9b87d9046&users_list=All&view_type=month&date='+mm+'-'+dd+'-'+year+'&bill_status=All&component_type='+projdata.Component_Type__c;
            system.debug('url--------------------> '+url);
            //Http requet to get timehseet data
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            request.setEndpoint(url);
            request.setMethod('GET');
            HttpResponse response = new HttpResponse();
            //added for test class
            String jsonResult;
            if(!test.isrunningtest()){ 
                response = http.send(request);
                String contentType = response.getHeader('Content-Type');
                jsonResult = response.getBody();
            }
            
            else{
                //mock response for test class
                string s = '/';
                jsonResult = '{"timelogs":{"role":"admin","date":[{"display_format":"05-20-2016 12:00:00 AM","tasklogs":[{"id":187384000001813013,"minutes":0,"owner_name":"arun.prasad","task":{"id":187384000001706580,"name":"Project Management"},"hours":1,"link":{"self":{"url":"https://projectsapi.zoho.com/restapi/portal/kvpbusiness/projects/187384000000250213/tasks/187384000001706580/logs/187384000001813013/"}},"approval_status":"Unapproved","owner_id":"61706953","total_minutes":60,"bill_status":"Billable","hours_display":"01:00","notes":"Task Updates Project tracking"},{"id":187384000001820049,"minutes":24,"owner_name":"disha.shetty","task":{"id":187384000001725215,"name":"Testing"},"hours":0,"link":{"self":{"url":"https://projectsapi.zoho.com/restapi/portal/kvpbusiness/projects/187384000000250213/tasks/187384000001725215/logs/187384000001820049/"}},"approval_status":"Unapproved","owner_id":"61783905","total_minutes":24,"bill_status":"Billable","hours_display":"00:24","notes":"Bulk Clone Opportunity- a scenario discussion about the closed year of the cloned Opportunity with Keyur. Clarification been given to Keyur."}],"date":"05-20-2016","total_hours":"1:24","date_long":1463682600000}],"grandtotal":"124:34"}}';
            }
            
            // system.debug('json'+jsonResult);
            
            //map to get data in object
            Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(jsonResult);        
            //map to get data from above map
            Map<String, Object> m1 = (Map<String, Object>)m.get('timelogs');        
            //list to get object data out of map
            List<Object> a = (List<Object>)m1.get('date'); 
            
            
            //iterate object to get timesheet records
            for(object pr : a){
                Map<String, Object> val = (Map<String, Object>) pr;
                system.debug('******val*****'+val);
                list<Object> excep = new list<object>();
                system.debug('******projdata.Component_Type__c*****'+projdata.Component_Type__c);
                if(projdata.Component_Type__c == 'task')
                    excep = (List<Object>)val.get('tasklogs'); 
                else if(projdata.Component_Type__c == 'bug')
                    excep = (List<Object>)val.get('buglogs');      
                else if(projdata.Component_Type__c == 'general')
                    excep = (List<Object>)val.get('generallogs');      
                if(excep != NULL && !excep.isEmpty()){
                    system.debug('***********'+excep); 
                    system.debug('******excep.size()*****'+excep.size());
                    for(object exc : excep){
                        system.debug('*****exc******'+exc); 
                        Timesheet__c ts = new Timesheet__c();
                        Map<String, Object> exVal = (Map<String, Object>) exc; 
                        
                        ts.LOG_ID__c = string.ValueOf(exVal.get('id'));
                        system.debug('ts.LOG_ID__c'+ts.LOG_ID__c);
                        ts.Owner_ID__c = string.ValueOf(exVal.get('owner_id'));
                        ts.Log_Notes__c = string.ValueOf(exVal.get('notes'));
                        ts.Project_ID__c = projdata.Project_Id__c;
                        ts.Hours__c = Double.ValueOf(exVal.get('total_minutes'))/60;
                        ts.Bill_Status__c = string.ValueOf(exVal.get('bill_status'));
                        string ownerId = string.ValueOf(exVal.get('owner_id'));
                        string ownername = string.ValueOf(exVal.get('owner_name'));
                        
                        system.debug('ownerId---owner_name----------------------------- >'+ownerId+'-----'+ownername);
                        
                        ts.Resource_Name__c = resourceIdMap.get(ownerId);
                        
                        if(ts.Resource_Name__c == NULL)
                            ts.Resource_Name__c = res.Id;
                        ts.Project_Name__c = projdata.id;
                        
                        string time_dat = String.ValueOf(Val.get('date'));
                        Integer time_mon = Integer.valueOf(time_dat.substring(0,2));
                        Integer time_da = Integer.valueOf(time_dat.substring(3,5));
                        Integer time_year = Integer.valueOf(time_dat.substring(6,10));
                        
                        Date timesheet_date = Date.newInstance(time_year,time_mon,time_da);
                        ts.Date__c = timesheet_date;
                        
                        time_list.add(ts);
                        system.debug('time_list--------------------------->'+time_list);
                        
                    }
                }
                
            }
            
        }//end of for loop
        if(!time_List.isEmpty()) Upsert time_List LOG_ID__c ;
    }   
    
    global void finish(Database.BatchableContext BC){
        
        
    }
    
}