@isTest
public class EventTriggerHandler_Test {
    
    public static testmethod void EventTriggerTest()
    {
        account acc = new account();
        acc.Name = 'lkj1';
        acc.CurrencyIsoCode='USD';
        insert acc;
       Opportunity__c opp1 = new Opportunity__c();
        opp1.Name='abc3';
        opp1.Account__c= acc.id;
        opp1.Opportunity_Category__c	='Existing Customer - Existing Opportunity';
        opp1.Type__c='Ingram License';
        opp1.Stage__c='Identified';
        opp1.Sub_stage__c='Opportunity received';
        opp1.Close_Date__c=date.today();
        insert opp1;
        Opportunity__c opp2= new Opportunity__c();
        opp2.name='abc4';
        opp2.Account__c=acc.id;
        opp2.Opportunity_Category__c	='Existing Customer - Existing Opportunity';
        opp2.Type__c='Ingram License';
        opp2.Stage__c='Identified';
        opp2.Sub_stage__c='Opportunity received';
        opp2.Close_Date__c=date.today();
        insert opp2;
        Event e1=new Event();
        e1.whatId = opp1.id;
        //e1.DurationInMinutes = 9;
       // e1.ActivityDateTime = dateTime.now();
        e1.Subject='Call';
        e1.StartDateTime=dateTime.now();
        e1.EndDateTime=dateTime.now();
        e1.CurrencyIsoCode='USD';
        insert e1;
        Event e2=new Event();
        e2.WhatId=opp2.id;
        //e2.DurationInMinutes = 8;
       // e2.ActivityDateTime = dateTime.now();
        e2.Subject='Meeting';
        e2.StartDateTime=dateTime.now();
        e2.EndDateTime=dateTime.now();
        e2.CurrencyIsoCode='USD';
        insert e2;
        Event e=new Event();
        e=[SELECT id, whatId FROM Event WHERE WhatId = :opp2.id];
        e.WhatId=opp1.id;
        update e;
        delete e1;
    }

}