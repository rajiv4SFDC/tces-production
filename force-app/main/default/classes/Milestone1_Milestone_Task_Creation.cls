public class Milestone1_Milestone_Task_Creation {
  
   public static void doHandler()
   {
   
           System.debug('sandbox context');
   }

 public static void MilestoneTasks(List<Milestone1_Milestone__c> listMilestone){
        
        List<Milestone1_Task__c> listTask = new List<Milestone1_Task__c>();
        
        for(Milestone1_Milestone__c mil : listMilestone){
        
          Milestone1_Task__c t1 = new Milestone1_Task__c();
          Milestone1_Task__c t2 = new Milestone1_Task__c();
          Milestone1_Task__c t3 = new Milestone1_Task__c();
          Milestone1_Task__c t4 = new Milestone1_Task__c();
          Milestone1_Task__c t5 = new Milestone1_Task__c();
        
        if(mil.Name=='General Milestone'){
         
            t1.Project_Milestone__c = mil.id;
            t1.name = 'Leave Task';
            t1.Type_Of_Task__c = 'Plan and analyse';
            t1.Priority__c = '0';
            t1.Description__c = 'Leave Task';
            t1.Start_Date1__c = mil.Kickoff__c;
            t1.Due_Date1__c = mil.Deadline__c;
            t1.Prior__c = 'P3-Low';
            listTask.add(t1);
            
            t2.Project_Milestone__c = mil.id;
            t2.name = 'Project Bench';
            t2.Type_Of_Task__c = 'Plan and analyse';
            t2.Priority__c = '0';
            t2.Description__c = 'Project Bench';
            t2.Start_Date1__c = mil.Kickoff__c;
            t2.Due_Date1__c = mil.Deadline__c;
            t2.Prior__c = 'P3-Low';
            listTask.add(t2);
            
            t3.Project_Milestone__c = mil.id;
            t3.name = 'Code Review';
            t3.Type_Of_Task__c = 'Plan and analyse';
            t3.Priority__c = '0';
            t3.Description__c = 'Code Review';
            t3.Start_Date1__c = mil.Kickoff__c;
            t3.Due_Date1__c = mil.Deadline__c;
            t3.Prior__c = 'P3-Low';
            listTask.add(t3);
            
            t4.Project_Milestone__c = mil.id;
            t4.name = 'Sales Support';
            t4.Type_Of_Task__c = 'Plan and analyse';
            t4.Priority__c = '0';
            t4.Description__c = 'Sales Support';
            t4.Start_Date1__c = mil.Kickoff__c;
            t4.Due_Date1__c = mil.Deadline__c;
            t4.Prior__c = 'P3-Low';
            listTask.add(t4);
            
            t5.Project_Milestone__c = mil.id;
            t5.name = 'Technical Support';
            t5.Type_Of_Task__c = 'Plan and analyse';
            t5.Priority__c = '0';
            t5.Description__c = 'Technical Support';
            t5.Start_Date1__c = mil.Kickoff__c;
            t5.Due_Date1__c = mil.Deadline__c;
            t5.Prior__c = 'P3-Low';
            listTask.add(t5);
            }
        
        if(mil.Name=='Project Initiation Documentation'){
         
            t1.Project_Milestone__c = mil.id;
            t1.name = 'Technical Architecture Document';
            t1.Type_Of_Task__c = 'Plan and analyse';
            t1.Priority__c = '0';
            t1.Description__c = 'Technical Architecture Document';
            t1.Start_Date1__c = mil.Kickoff__c;
            t1.Due_Date1__c = mil.Deadline__c;
            t1.Prior__c = 'P3-Low';
            listTask.add(t1);
            
            t2.Project_Milestone__c = mil.id;
            t2.name = 'Requirement Sign-off Document';
            t2.Type_Of_Task__c = 'Plan and analyse';
            t2.Priority__c = '0';
            t2.Description__c = 'Requirement Sign-off Document';
            t2.Start_Date1__c = mil.Kickoff__c;
            t2.Due_Date1__c = mil.Deadline__c;
            t2.Prior__c = 'P3-Low';
            listTask.add(t2);
            
            t3.Project_Milestone__c = mil.id;
            t3.name = 'UI-Mock-up Design Document';
            t3.Type_Of_Task__c = 'Plan and analyse';
            t3.Priority__c = '0';
            t3.Description__c = 'UI-Mock-up Design Document';
            t3.Start_Date1__c = mil.Kickoff__c;
            t3.Due_Date1__c = mil.Deadline__c;
            t3.Prior__c = 'P3-Low';
            listTask.add(t3);
            
            t4.Project_Milestone__c = mil.id;
            t4.name = 'Process Document';
            t4.Type_Of_Task__c = 'Plan and analyse';
            t4.Priority__c = '0';
            t4.Description__c = 'Process Document';                                           
            t4.Start_Date1__c = mil.Kickoff__c;
            t4.Due_Date1__c = mil.Deadline__c;
            t4.Prior__c = 'P3-Low';
            listTask.add(t4);
        }
        if(mil.Name=='Project Closure Documentation'){
         
            t1.Project_Milestone__c = mil.id;
            t1.name = 'UAT Document';
            t1.Type_Of_Task__c = 'Plan and analyse';
            t1.Priority__c = '0';
            t1.Description__c = 'UAT Document';
            t1.Start_Date1__c = mil.Kickoff__c;
            t1.Due_Date1__c = mil.Deadline__c;
            t1.Prior__c = 'P3-Low';
            listTask.add(t1);
            
            t2.Project_Milestone__c = mil.id;
            t2.name = 'Training Document';
            t2.Type_Of_Task__c = 'Plan and analyse';
            t2.Priority__c = '0';
            t2.Description__c = 'Training Document';
            t2.Start_Date1__c = mil.Kickoff__c;
            t2.Due_Date1__c = mil.Deadline__c;
            t2.Prior__c = 'P3-Low';
            listTask.add(t2);
            
            t3.Project_Milestone__c = mil.id;
            t3.name = 'Project Sign-Off';
            t3.Type_Of_Task__c = 'Plan and analyse';
            t3.Priority__c = '0';
            t3.Description__c = 'Project Sign-Off';
            t3.Start_Date1__c = mil.Kickoff__c;
            t3.Due_Date1__c = mil.Deadline__c;
            t3.Prior__c = 'P3-Low';
            listTask.add(t3);
            
        }
        }
        insert listTask;
       }

}