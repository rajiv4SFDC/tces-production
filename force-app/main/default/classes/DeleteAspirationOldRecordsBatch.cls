/******************************************
 * Created By  : Ajeet Singh Shekhawat
 * Created On  : 18 Sep 2018
 * Modified By : Ajeet Singh Shekhawat
 * Modified On : 18 Sep 2018
 * Description : Delete duplicate/old records which have DisplayRecord__c = true.
				
******************************************/
global class DeleteAspirationOldRecordsBatch implements Database.Batchable<Sobject> {
    // Start method
	global Database.QueryLocator start(Database.BatchableContext BaCon) {
        Boolean displayRecords = True;
        return Database.getQueryLocator('Select id, DisplayRecord__c From Aspirations_Skill__c Where DisplayRecord__c =: displayRecords'); // fetch records which have DisplayRecord as true means these records are old records 
    }
    
    global void execute(Database.BatchableContext BaCon, List<Aspirations_Skill__c> aspList) {        
        try {
            System.debug('aspList is ' + aspList);
			delete aspList; // delete old records
            System.debug('aspList is ' + aspList);
        }
        catch(DmlException ex) {
            System.debug('Exception occured: ' + ex);
        }
    }
    
    global void finish(Database.BatchableContext BaCon) {
        
    }
    /*Schedule method      
    public void execute(SchedulableContext sc){
        DeleteAspirationOldRecordsBatch aspDelete = new DeleteAspirationOldRecordsBatch();
        Database.executeBatch(aspDelete, 200);
    }*/
}