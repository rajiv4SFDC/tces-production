/***********************************************************
* Created By:   Ankit Kumar(KVP Business Solutions)
* Created Date: 12/June/2018
* Modified By : 
* Description : This is to insert the candidate from site. 
************************************************************/
public class CandidatePageController {
  public Candidate__c can {get;set;}
  public Blob FileBody    {get;set;}
  public String FileName  {get;set;}  
    public CandidatePageController(){      
    System.debug('Iam in my constructor'); 
        Id RecordTypeId = Schema.SObjectType.Candidate__c.getRecordTypeInfosByName().get('Online Candidate').getRecordTypeId();
        can             = new Candidate__c();
        can.RecordTypeId= RecordTypeId;
    }
    public PageReference doSaveData(){
     system.debug('******coming*******');
     Insert can;    
     Attachment attach;       
     if((FileBody !=NULL && FileName !=NULL) || test.isRunningTest()){    
          attach= new Attachment();
          attach.Body=FileBody;
          attach.Name=FileName;
          attach.IsPrivate=True;
          attach.ParentId=can.Id;
      }
     try{         
          Insert attach;
      }catch(Exception e){          
          System.debug('---Exception---->'+e.getMessage()+'at line number---->'+e.getLineNumber());
      }       
    PageReference pageRef = new PageReference('/apex/RecordSavedPage');   
    return pageRef;  
    }    
  }