global class  mailalerts implements Schedulable {
global void execute(SchedulableContext ctx) {

sendmail('lagnajeeta.biswal@kvpcorp.com','Lagnajeeta');
sendmail('rakesh.s@kvpcorp.com','Rakesh');
sendmail('santosh.murthy@kvpcorp.com','Santosh');
sendmail('aniruddha.saha@kvpcorp.com','Aniruddha');
sendmail('ranveer.barooah@kvpcorp.com','Ranveer');
sendmail('roshan.khotele@kvpcorp.com','Roshan');
     
     
   }
   
 global void sendmail(String emails,string name)
 {
 Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
String[] toAddresses = new String[] {emails}; 
String[] ccAddresses = new String[] {'kumar.guha@kvpcorp.com'};
String[] BccAddresses = new String[] {'rahul.baliga@kvpcorp.com'};
  

mail.setToAddresses(toAddresses);
mail.setCcAddresses(ccAddresses);
mail.setBccAddresses(BccAddresses);

mail.setReplyTo('varun_nagpal@kpcorp.com');

mail.setSenderDisplayName('PMS Admin');

mail.setSubject('Revenue Schedule alert');

mail.setBccSender(false);

mail.setUseSignature(false);

mail.setPlainTextBody('Revenue Schedule for the current month');

mail.setHtmlBody('Dear'+''+name+'<p>'+
'We are nearing our invoice cycle for the current month & would require to update the invoice details for your respective projects accordingly.<p>'+
'Please log into your PMS & update the same at the earliest. You can also log into the below link to access your reports.<p>'
+' <a href=https://ap1.salesforce.com/00O90000003E0Ym?pv0='+name+'>click here.</a>');

Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
 
 }  
      
   }