@isTest
public class MilestoneDetailedTest {
    static testMethod void pdf(){
        
        Milestone1_Task__c c1;
        Account acc=new Account();
        acc.Name='KVP Testing Account';
        insert acc;
        
        Contact con=new Contact();
        con.LastName='Test Contact';
        con.AccountId=acc.id;
        con.Email='xyz@gmail.com';
        insert con;
        
        Opportunity__c opp=new Opportunity__c();
        opp.Name='KVP Testing Account Opportunity';
        opp.Account__c = acc.Id;
        opp.Opportunity_Category__c='New Customer - New Opportunity';
        opp.Type__c='Services';
        opp.Close_Date__c=System.today();
       // opp.First_Point_of_Escalation_Contact_Email__c=con.Id;
        //opp.Second_Point_of_Escalation_Contact_Email__c=con.Id;
        insert opp;
        
        system.debug('op'+opp);
        Milestone1_Project__c project=new Milestone1_Project__c();
        project.Name='KVP Test Class Project';
        //project.Account__c=acc.Id;
        project.Opportunity__c=opp.Id;
        project.Milestone_Frequency__c='Month';
        project.Total_Hours_Estimate__c=450;
        project.Risks_In_The_Project__c='Low';
        project.People_Risks_In_The_Project__c='Low';
        project.Project_Complexity__c='Low';
        project.Actual_Start_Date__c=System.today();
        project.Actual_End_Date__c=System.today();
        insert project;
        
        Milestone1_Milestone__c	miles=new Milestone1_Milestone__c();
        miles.Name='KVP Milestone Test Class';
        miles.Project__c = project.id;
        miles.Focus_of_this_milestone__c='aa';
        miles.Kickoff__c=System.today();
        miles.Deadline__c=System.today();
        insert miles;
        
        Id RecordTypeIdA2Planned = Schema.SObjectType.Milestone1_Task__c.getRecordTypeInfosByName().get('A-Planned').getRecordTypeId();
        Milestone1_Task__c reqA2 = new Milestone1_Task__c();
        reqA2.Name='A-Planned Req2';
        reqA2.Project_Milestone__c=miles.id;
        reqA2.Assigned_To__c=UserInfo.getUserId();
        reqA2.Risk_Escalation__c='Very critical';
        reqA2.Description__c='To check if data points are coming or not';
        reqA2.Start_Date1__c=System.today();
        reqA2.Due_Date1__c=System.today();
        reqA2.RecordTypeId=RecordTypeIdA2Planned;
        insert reqA2;
        
        Id RecordTypeIdAPlanned = Schema.SObjectType.Milestone1_Task__c.getRecordTypeInfosByName().get('A-Planned').getRecordTypeId();
        Milestone1_Task__c reqA = new Milestone1_Task__c();
        reqA.Name='A-Planned Req';
        reqA.Project_Milestone__c=miles.id;
        reqA.Assigned_To__c=UserInfo.getUserId();
        reqA.Risk_Escalation__c='Very critical';
        reqA.Description__c='To check if data points are coming or not';
        reqA.Start_Date1__c=System.today();
        reqA.Due_Date1__c=System.today();
        reqA.RecordTypeId=RecordTypeIdAPlanned;
        insert reqA;
        
        Milestone1_Task__c req=new Milestone1_Task__c();
        req.Name='KVP Test UAT';
        req.Project_Milestone__c=miles.Id;
        req.Predecessor_Task__c=reqA.Id;
        req.Assigned_To__c=UserInfo.getUserId();
        req.Test_Case_Type__c='Positive';
        req.UAT_Test_Case__c=true;
        req.Test_Description__c='x';
        req.Test_Steps__c='a';
        req.Expected_Results__c='a';
        insert req;  
        system.debug('req'+req);
        
        Id RecordTypeIdContact = Schema.SObjectType.Milestone1_Task__c.getRecordTypeInfosByName().get('F-Changed Request (CR)').getRecordTypeId();
        System.debug('Record id-->>>'+RecordTypeIdContact);
        
        Milestone1_Task__c req1=new Milestone1_Task__c();
        req1.Name='KVP Test UAT1';
        req1.Project_Milestone__c=miles.Id;
        req1.Assigned_To__c=UserInfo.getUserId();
        req1.Schedule_impact__c=2;
        req1.Effort_impact__c=2;
        req1.Budget_impact__c=2;
        req1.RecordTypeId=RecordTypeIdContact;
        req1.Start_Date1__c=System.today();
        req1.Due_Date1__c=System.today();
        req1.Description__c='abc';
        req1.Rationale_for_this_change__c='abc';
        insert req1;
        
        system.debug('cr-->>'+req1);
        system.debug('miles : '+miles);
        //ApexPages.StandardController sc = new ApexPages.StandardController(miles);
        //pdf_for_Milestones_with_Requirements ttt = new pdf_for_Milestones_with_Requirements(sc);
        //pdf_for_Milestones_with_Requirements controller = new pdf_for_Milestones_with_Requirements() ;
        Test.startTest();
        Test.setCurrentPageReference(new PageReference('Page.MilestoneDetailedReport')); 
        System.currentPageReference().getParameters().put('id', miles.Id);
        ApexPages.StandardController sc = new ApexPages.standardController(miles);
        
        MilestoneDetailedController controller = new MilestoneDetailedController(sc);
        
        Test.stopTest();
        
        
    }
}