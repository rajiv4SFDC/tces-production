/**************************************************************************************************************************************************************************************
* Company:      KVP Business Solutions Pvt. Ltd
* Created By:   Subhajit Ghosh
* Created Date: 15.01.2019 
* Description:  This is a apex class that generates PDF for the Milestone object along with 
A-Planned Tasks, Ad-Hoc Tasks, C-Test Tasks, CR Tasks. It is the controller to "pdf_for_Milestones_with_Requirement_VF" VF Page  
*******************************************************************************************************************************************************************************************/




public class pdf_for_Milestones_with_Requirements {
    
    public List<Milestone1_Task__c> mileTaskList{set;get;}
    public List<Milestone1_Task__c> mileTestCaseTaskList{set;get;}
    public List<Milestone1_Task__c> mileTestTaskUATList{set;get;}
    public List<Line_Items__c> projectOwnerDevList{set;get;}
    public List<Milestone1_Milestone__c> projectIdList{set;get;}
    public List<Milestone1_Project__c> ProjectOwnerInfo{set;get;}
    public List<Milestone1_Task__c> mileCR_TaskList{set;get;}
    public List<Event> eventList{get;set;}
    public string ProjectId;
    public boolean x{set;get;}
    public boolean y{set;get;}
    //public boolean projectOwnerDevListCount{set;get;}
    public boolean mileCR_TaskListCount{set;get;}
    public Map<DateTime,String> eventMap{set;get;}
    public List<Event> eventRecordList{set;get;}
    public boolean eventCount{get;set;}
    public boolean TestUATCount{get;set;}
    public Integer uatSize{get;set;}
    public Integer milestoneTaskCount{set;get;}
    public Integer totalReqCount{set;get;}
    public Map<String,List<Milestone1_Task__c>> mileTaskMap{set;get;}
    
    
    
    
    
    
    public pdf_for_Milestones_with_Requirements(ApexPages.StandardController stdController){
        System.debug('stdController : '+stdController);
        mileTaskList=new List<Milestone1_Task__c>();
        mileTestCaseTaskList=new List<Milestone1_Task__c>();
        mileTestTaskUATList=new List<Milestone1_Task__c>();
        projectOwnerDevList=new List<Line_Items__c>();
        projectIdList=new List<Milestone1_Milestone__c>();
        ProjectOwnerInfo=new List<Milestone1_Project__c>();
        mileCR_TaskList=new List<Milestone1_Task__c>();
        eventList=new List<Event>();
        eventMap=new Map<DateTime,String>();
        eventRecordList=new List<Event>();
        mileTaskMap=new Map<String,List<Milestone1_Task__c>>();
        
        //dateTimeVal=new List<DateTime>();
        
        
        mileTaskList=[SELECT Name,Start_Date1__c,Due_Date1__c,Task_Stage__c,Assigned_To__r.Name,RecordType.Name,Project_Module__r.Name,Task_Completion__c
                      FROM Milestone1_Task__c where Project_Milestone__c =: ApexPages.currentPage().getParameters().get('id')
                      AND (RecordType.Name =:'A-Planned' OR RecordType.Name =:'B-Ad-Hoc') 
                      AND Name !='Daily Scrum Meeting' AND Name !='Customer Review Meeting' AND Name !='Internal Review Meeting' 
                      Order by Task_Completion__c DESC,Start_Date1__c];
        
        for(Milestone1_Task__c mmt:mileTaskList){
            if(mileTaskMap.containsKey(mmt.Project_Module__r.Name)){
                mileTaskMap.get(mmt.Project_Module__r.Name).add(mmt);
            }
            else{
                mileTaskMap.put(mmt.Project_Module__r.Name,new List<Milestone1_Task__c> {mmt});
            }
            
        }
        System.debug('Value in Map++>>'+mileTaskMap);
        
        
        
        milestoneTaskCount=mileTaskList.size();
        
        if(mileTaskList.size()>0 || !mileTaskList.isEmpty()){
            y=true;
        }
        
        
        mileTestCaseTaskList=[SELECT Name,Start_Date1__c,Due_Date1__c,Task_Stage__c,Assigned_To__r.Name,RecordType.Name,Test_Case_Type__c
                              FROM Milestone1_Task__c where Project_Milestone__c =: ApexPages.currentPage().getParameters().get('id')
                              AND RecordType.Name =:'C-Test Case'];
        totalReqCount=mileTaskList.size() + mileTestCaseTaskList.size();
        if(mileTestCaseTaskList.size()>0){
            x=true;
        }
        
        
        mileTestTaskUATList=[SELECT Name,Start_Date1__c,Due_Date1__c,Task_Stage__c,Assigned_To__r.Name,RecordType.Name,Test_Case_Type__c
                             FROM Milestone1_Task__c where Project_Milestone__c =:ApexPages.currentPage().getParameters().get('id')
                             AND RecordType.Name =:'C-Test Case' AND UAT_Test_Case__c=true];
        
        uatSize=mileTestTaskUATList.size();
        if(mileTestTaskUATList.size()>0){
            
            TestUATCount=true;
        }
        
        projectIdList=[SELECT project__c FROM Milestone1_Milestone__c WHERE id =:ApexPages.currentPage().getParameters().get('id')];
        System.debug('projectIdList:' +projectIdList);
        ProjectId=projectIdList[0].project__c;
        System.debug(ProjectId);
        
        projectOwnerDevList=[select id,User__r.name,Project__r.name,Project__r.Owner_Name__c,Role__c from Line_Items__c where Project__c =:ProjectId AND Role__c!='Project Manager'];
        System.debug('projectOwnerDevList '+projectOwnerDevList);
        /* if(projectOwnerDevList.size()>0){
projectOwnerDevListCount=true;

}*/
        
        
        
        ProjectOwnerInfo=[SELECT Owner_Name__c,Owner.Email,Owner.Phone,Name from Milestone1_Project__c WHERE id =:ProjectId];
        
        
        mileCR_TaskList=[SELECT Name,Start_Date1__c,Due_Date1__c,Task_Stage__c,Assigned_To__r.Name,Impact_of_this_CR__c	
                         FROM Milestone1_Task__c where Project_Milestone__c =: ApexPages.currentPage().getParameters().get('id')
                         AND RecordType.Name =:'F-Changed Request (CR)'];
        if(mileCR_TaskList.size()>0){
            mileCR_TaskListCount=true;
        }
        
        
        /* eventList=[select id,Subject,StartDateTime,EndDateTime,Location,LastModifiedDate,MOM_Link__c from Event where whatid=:ApexPages.currentPage().getParameters().get('id')];
System.debug(eventList);
if(eventList.size()>0){
eventCount=true;
}

for(Event e:eventList){

if(!eventMap.containsKey(e.LastModifiedDate)){
eventMap.put(e.LastModifiedDate,e.Location);
eventRecordList.add(e);

}
/*  if(!eventSet.contains(e.whatid+e.Subject)){
eventSet.add(e.whatid+e.Subject);
eventRecordList.add(e);

//  }

}*/
        String myGeneratedFileName ='KVP_'+ProjectOwnerInfo[0].Name+'_Milestone'+'.pdf';
        Apexpages.currentPage().getHeaders().put('content-disposition', 'inline; filename='+myGeneratedFilename); 
        
        
    }
    
    
}