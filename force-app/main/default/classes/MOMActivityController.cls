public class MOMActivityController {
    
    public List<Task> TaskRecordList{get;set;}
    public Map<DateTime,String> TaskMap{set;get;}
    public List<Task> TaskList{get;set;}
    
    
    public MOMActivityController(ApexPages.StandardController controller){
        TaskRecordList=new List<Task>();
        TaskList=new List<Task>();
        TaskMap=new Map<DateTime,String>();
        
        
        TaskList=[SELECT Subject,ActivityDate,Status,LastModifiedDate FROM Task where whatid=:ApexPages.currentPage().getParameters().get('id') ORDER BY ActivityDate];
        for(Task t:TaskList){
            
            if(!TaskMap.containsKey(t.LastModifiedDate)){
                TaskMap.put(t.LastModifiedDate,t.Status);
                TaskRecordList.add(t);
                
            }
        }
        
        
    }
}