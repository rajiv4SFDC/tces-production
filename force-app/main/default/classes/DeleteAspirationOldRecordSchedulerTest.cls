/******************************************
 * Created By  : Ajeet Singh Shekhawat
 * Created On  : 18 Sep 2018
 * Modified By : Ajeet Singh Shekhawat
 * Modified On : 18 Sep 2018
 * Description : 
				
******************************************/
@isTest
public class DeleteAspirationOldRecordSchedulerTest {
    public static testMethod void DeleteAspirationOldRecordSchedulerTest() {
        Aspirations_Skill__c asp = new Aspirations_Skill__c();
        asp.DisplayRecord__c = true;
        Insert asp; // inserting records
        Test.StartTest(); 
            DeleteAspirationOldRecordScheduler aspDelete = new DeleteAspirationOldRecordScheduler();
            String sch = '0 0 23 * * ?'; // schedule class to run at particular time.
            System.schedule('Old record delete', sch, aspDelete);
        Test.StopTest();    
    }
}