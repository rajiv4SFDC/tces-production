@isTest
private class LeadStatusupdatehandler_Test{
  @testSetup
  static void setupTestData(){
    test.startTest();
    Enquiry__c enquiry_Obj = new Enquiry__c(Is_Converted__c = false, Lead_Converted__c = false,Enquiry_Status__c= 'Qualify Database');
    Insert enquiry_Obj; 
    Task task_Obj = new Task(Status = 'Completed', Priority = 'High', IsReminderSet = false, IsRecurrence = false,whatid =enquiry_Obj.id);
    Insert task_Obj; 
    test.stopTest();
  }
  static testMethod void test_UpdateLeadStatus_UseCase1(){
    Map<id,Enquiry__c> maplead = new Map<id,Enquiry__c>(); 
    List<Enquiry__c> enquiry_Obj  =  [SELECT id,Is_Converted__c,Lead_Converted__c,Enquiry_Status__c from Enquiry__c];
    System.assertEquals(true,enquiry_Obj.size()>0);
    List<Task> task_Obj  =  [SELECT Status,Priority,IsReminderSet,IsRecurrence,whatId from Task];
    System.assertEquals(true,task_Obj.size()>0);
   // if(task_Obj!=null){
    LeadStatusupdatehandler obj01 = new LeadStatusupdatehandler();
    obj01.UpdateLeadStatus(enquiry_Obj,maplead);
  //}
  }
}