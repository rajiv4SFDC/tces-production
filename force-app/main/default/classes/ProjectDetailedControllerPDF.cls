/*********************************************************************************************************************************************************************************************************
* Company:      KVP Business Solutions Pvt. Ltd
* Created By:   Subhajit Ghosh
* Created Date: 23.01.2019
* Description:  This is a VF page that generates PDF in the Project level giving detailed description about the requirements,test cases,modules

***********************************************************************************************************************************************************************************************************/

public class ProjectDetailedControllerPDF {
    public List<Milestone1_Milestone__c> milestoneID{set;get;}
    public List<Milestone1_Task__c> projectDetailedList{set;get;} 
    public List<Line_Items__c> teamInfo{set;get;}
    public List<Milestone1_Project__c> ownerInfo{set;get;}
    public Map<Id,List<Milestone1_Task__c>> reqWithManyTestCasesMap{set;get;}
    public List<Milestone1_Task__c> reqWithoutTC{set;get;}
    public Set<Id> predecessorId{set;get;}
    public List<User_Story1__c> userStoryList{set;get;}
    public List<Milestone1_Project__c> projectModulesCount{set;get;} 
    public List<AggregateResult> reqUnderModuleList{set;get;}
    public List<Milestone1_Project__c> reqUnderModuleList1{set;get;} 
    
    public ProjectDetailedControllerPDF(ApexPages.StandardController stdController){
        milestoneID=new List<Milestone1_Milestone__c>();
        projectDetailedList=new List<Milestone1_Task__c>();
        reqWithManyTestCasesMap=new Map<Id,List<Milestone1_Task__c>>();
        reqWithoutTC=new List<Milestone1_Task__c>();
        predecessorId=new Set<Id>();
        userStoryList=new List<User_Story1__c>();
        projectModulesCount=new List<Milestone1_Project__c>();
        reqUnderModuleList=new List<AggregateResult>();
        reqUnderModuleList1=new List<Milestone1_Project__c>();
        ownerInfo=new List<Milestone1_Project__c>();
        teamInfo=new List<Line_Items__c>();
        
        milestoneID=[SELECT id from Milestone1_Milestone__c where Project__c =: ApexPages.currentPage().getParameters().get('id')];
        
        projectDetailedList=[SELECT Name,Start_Date1__c,Due_Date1__c,Task_Stage__c,Assigned_To__r.Name,Test_Case_Type__c,Test_Description__c,Test_Steps__c,Expected_Results__c,Predecessor_Task__r.Id,
                             Predecessor_Task__r.Name,Predecessor_Task__r.Project_Module__r.Name,Predecessor_Task__r.Assigned_To__r.Name,Predecessor_Task__r.Risk_Escalation__c,Predecessor_Task__r.Description__c,
                             Predecessor_Task__r.Start_Date1__c,Predecessor_Task__r.Due_Date1__c,Predecessor_Task__r.Task_Stage__c,Predecessor_Task__r.Task_idea_source__c,Project_Milestone__r.Name,Project_Milestone__r.Project__r.Name,Project_Milestone__r.Milestone_ID__c
                             FROM Milestone1_Task__c where Project_Milestone__c in : milestoneID AND Project_Milestone__r.Name != 'General Milestone' AND Predecessor_Task__r.Name != 'Internal Review Meeting' AND Predecessor_Task__r.Name != 'Customer Review Meeting' AND Predecessor_Task__r.Name != 'Daily Scrum Meeting'
                             AND Predecessor_Task__c!=NULL AND RecordType.Name =:'C-Test Case' AND (Predecessor_Task__r.RecordType.Name =: 'A-Planned' 
                                                                                                    OR Predecessor_Task__r.RecordType.Name =:'B-Ad-Hoc') ORDER BY Project_Milestone__r.Name];
        
        for(Milestone1_Task__c req : projectDetailedList){
            if(reqWithManyTestCasesMap.containsKey(req.Predecessor_Task__r.Id)){
                reqWithManyTestCasesMap.get(req.Predecessor_Task__r.Id).add(req);
            }
            else{
                reqWithManyTestCasesMap.put(req.Predecessor_Task__r.Id,new List<Milestone1_Task__c> {req});
            }
            
        }
        
        
        SYSTEM.debug('map vALUES :'+reqWithManyTestCasesMap);
        
        for(Milestone1_Task__c task:projectDetailedList){
            predecessorId.add(task.Predecessor_Task__r.Id);
            
        }
        System.debug('setId'+predecessorId);
        
        reqWithoutTC=[SELECT Name,Start_Date1__c,Due_Date1__c,Task_Stage__c,Assigned_To__r.Name,RecordType.Name,Project_Module__r.Name,Task_idea_source__c,Description__c,Risk_Escalation__c,Project_Milestone__r.Name,Project_Milestone__r.Milestone_ID__c,
                      Project_Milestone__r.Kickoff__c,Project_Milestone__r.Deadline__c,Project_Milestone__r.Status__c FROM Milestone1_Task__c WHERE Project_Milestone__c in : milestoneID AND id NOT IN :predecessorId
                      AND (RecordType.Name =:'A-Planned' OR RecordType.Name =:'B-Ad-Hoc') AND Project_Milestone__r.Name != 'General Milestone'
                      AND Name !='Daily Scrum Meeting' AND Name !='Customer Review Meeting' AND Name !='Internal Review Meeting' 
                      Order by Project_Milestone__r.Name];
        
        userStoryList=[SELECT Use_Case_Title__c,Use_Case_criticality_To_Project__c,Pre_Conditions__c,Process_Steps__c,Use_Case_Documentation__c,Project_Acceptance_Criteria__c,Assumption_and_Risks__c,
                       Actors__c FROM User_Story1__c WHERE Project__c =:ApexPages.currentPage().getParameters().get('id')];
        
        projectModulesCount=[SELECT id FROM Milestone1_Project__c WHERE Project__c =: ApexPages.currentPage().getParameters().get('id') AND RecordType.Name = 'Project Module'];
        
        reqUnderModuleList=[SELECT Count(id) cid,Project_Module__r.Name nm FROM Milestone1_Task__c WHERE Project_Module__c IN : projectModulesCount AND Name !='Daily Scrum Meeting' AND Name !='Customer Review Meeting' AND Name !='Internal Review Meeting'
                            AND Project_Milestone__r.Name != 'General Milestone' AND (RecordType.Name =: 'A-Planned' OR  RecordType.Name =: 'B-Ad-Hoc') GROUP BY Project_Module__r.Name];
        System.debug('reqUnderModuleList'+ reqUnderModuleList);
        
        
        
        ownerInfo=[SELECT Owner_Name__c,Owner.Email,Owner.Phone,Name FROM Milestone1_Project__c where id =: ApexPages.currentPage().getParameters().get('id')];
        teamInfo=[SELECT User__r.Name,Role__c FROM Line_Items__c where Project__c=:ApexPages.currentPage().getParameters().get('id') AND Role__c!='Project Manager'];
        
    }
    
}