public class ProjectUATAllDetails {
 public List<Milestone1_Task__c> uatRequirementList{set;get;}
    public List<Milestone1_Milestone__c> milestoneIdList{set;get;}
    //public List<Milestone1_Task__c> UATCheckedbyCustomerList{set;get;}
    public String projectId{set;get;}
    public List<Line_Items__c> teamInfo{set;get;}
    public List<Milestone1_Project__c> ownerInfo{set;get;}
    public Boolean uatCount{set;get;} 
    public Integer uatSize{set;get;}
    
    
    public ProjectUATAllDetails(ApexPages.StandardController stdController){
      //  UATCheckedbyCustomerList=new List<Milestone1_Task__c>();
        uatRequirementList=new  List<Milestone1_Task__c>();
        milestoneIdList=new List<Milestone1_Milestone__c>();
        teamInfo=new List<Line_Items__c>();
        uatCount=false;
        
        milestoneIdList=[SELECT id from Milestone1_Milestone__c where Project__c =: ApexPages.currentPage().getParameters().get('id')];
        System.debug('UAT milestonePidList Case--->>'+milestoneIdList);
        
        
        uatRequirementList=[Select Name,Test_Case_Type__c,Project_Module__r.Name,
                                              Predecessor_Task__r.Name,Predecessor_Task__r.Description__c,Predecessor_Task__r.Assigned_To__r.Name,
                                                    Predecessor_Task__r.Short_ID__c,Test_Description__c,Assigned_To__r.Name,Test_Steps__c,Expected_Results__c,Start_Date1__c,Due_Date1__c
                                                                       FROM Milestone1_Task__c where Project_Milestone__c in: milestoneIdList AND Predecessor_Task__c != NULL AND UAT_Test_Case__c=TRUE ORDER BY Due_Date1__c];
         uatSize=uatRequirementList.size();
        if(uatRequirementList.size()>0){
            uatCount=true;
            }
        
        System.debug('UAT Test Case--->>'+uatRequirementList);
        
       
        
        
        ownerInfo=[SELECT Owner_Name__c,Owner.Email,Owner.Phone,Name FROM Milestone1_Project__c where id =: ApexPages.currentPage().getParameters().get('id')];
        teamInfo=[SELECT User__r.Name,Role__c FROM Line_Items__c where Project__c=:ApexPages.currentPage().getParameters().get('id') AND Role__c ='QA'];
        
    }
        
}