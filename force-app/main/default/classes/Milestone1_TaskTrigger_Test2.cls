@isTest
private class Milestone1_TaskTrigger_Test2{
    @testSetup
    static void setupTestData(){
        
        
    }
    static testMethod void test_Milestone1_TaskTrigger(){
        User user_Obj = new User(Username = 'TestUser846620181112140956@codecoverage.com',ProfileId='00e90000000o8Kt', LastName = 'LastName649', Email = 'Email19@test.com', EmailPreferencesAutoBcc = false, EmailPreferencesAutoBccStayInTouch = false, EmailPreferencesStayInTouchReminder = false, Alias = 'Alias942', CommunityNickname = 'cNickName87878', IsActive = true, TimeZoneSidKey = 'Pacific/Kiritimati', LocaleSidKey = 'sq', ReceivesInfoEmails = false, ReceivesAdminInfoEmails = false, EmailEncodingKey = 'UTF-8', CurrencyIsoCode = 'AUD', LanguageLocaleKey = 'en_US', UserPermissionsMarketingUser = false, UserPermissionsOfflineUser = false, UserPermissionsAvantgoUser = false, UserPermissionsCallCenterAutoLogin = false, UserPermissionsMobileUser = false, UserPermissionsSFContentUser = false, UserPermissionsInteractionUser = false, UserPermissionsSupportUser = false, UserPermissionsChatterAnswersUser = false, ForecastEnabled = false, UserPreferencesActivityRemindersPopup = false, UserPreferencesEventRemindersCheckboxDefault = false, UserPreferencesTaskRemindersCheckboxDefault = false, UserPreferencesReminderSoundOff = false, UserPreferencesDisableAllFeedsEmail = false, UserPreferencesDisableFollowersEmail = false, UserPreferencesDisableProfilePostEmail = false, UserPreferencesDisableChangeCommentEmail = false, UserPreferencesDisableLaterCommentEmail = false, UserPreferencesDisProfPostCommentEmail = false, UserPreferencesContentNoEmail = false, UserPreferencesContentEmailAsAndWhen = false, UserPreferencesApexPagesDeveloperMode = false, UserPreferencesHideCSNGetChatterMobileTask = false, UserPreferencesDisableMentionsPostEmail = false, UserPreferencesDisMentionsCommentEmail = false, UserPreferencesHideCSNDesktopTask = false, UserPreferencesHideChatterOnboardingSplash = false, UserPreferencesHideSecondChatterOnboardingSplash = false, UserPreferencesDisCommentAfterLikeEmail = false, UserPreferencesDisableLikeEmail = false, UserPreferencesSortFeedByComment = false, UserPreferencesDisableMessageEmail = false, UserPreferencesDisableBookmarkEmail = false, UserPreferencesDisableSharePostEmail = false, UserPreferencesEnableAutoSubForFeeds = false, UserPreferencesDisableFileShareNotificationsForApi = false, UserPreferencesShowTitleToExternalUsers = false, UserPreferencesShowManagerToExternalUsers = false, UserPreferencesShowEmailToExternalUsers = false, UserPreferencesShowWorkPhoneToExternalUsers = false, UserPreferencesShowMobilePhoneToExternalUsers = false, UserPreferencesShowFaxToExternalUsers = false, UserPreferencesShowStreetAddressToExternalUsers = false, UserPreferencesShowCityToExternalUsers = false, UserPreferencesShowStateToExternalUsers = false, UserPreferencesShowPostalCodeToExternalUsers = false, UserPreferencesShowCountryToExternalUsers = false, UserPreferencesShowProfilePicToGuestUsers = false, UserPreferencesShowTitleToGuestUsers = false, UserPreferencesShowCityToGuestUsers = false, UserPreferencesShowStateToGuestUsers = false, UserPreferencesShowPostalCodeToGuestUsers = false, UserPreferencesShowCountryToGuestUsers = false, UserPreferencesHideS1BrowserUI = false, UserPreferencesPathAssistantCollapsed = false, UserPreferencesCacheDiagnostics = false, UserPreferencesShowEmailToGuestUsers = false, UserPreferencesShowManagerToGuestUsers = false, UserPreferencesShowWorkPhoneToGuestUsers = false, UserPreferencesShowMobilePhoneToGuestUsers = false, UserPreferencesShowFaxToGuestUsers = false, UserPreferencesShowStreetAddressToGuestUsers = false, UserPreferencesLightningExperiencePreferred = false, UserPreferencesPreviewLightning = false, UserPreferencesHideEndUserOnboardingAssistantModal = false, UserPreferencesHideLightningMigrationModal = false, UserPreferencesHideSfxWelcomeMat = false, UserPreferencesHideBiggerPhotoCallout = false, UserPreferencesGlobalNavBarWTShown = false, UserPreferencesGlobalNavGridMenuWTShown = false, UserPreferencesCreateLEXAppsWTShown = false, UserPreferencesFavoritesWTShown = false, IsPortalSelfRegistered = false, DigestFrequency = 'D', DefaultGroupNotificationFrequency = 'P', select__c = false, Opt_Email_Alert__c = false, Total_Hours_Spent__c = 162, Total__c = 163);
        
        Insert user_Obj; 
        System.debug('@@@@User@@' +user_Obj);
        Milestone1_Project__c pm = new Milestone1_Project__c();
        pm.Name='test proj module';
        pm.Kickoff__c=system.today();
        pm.Deadline__c =system.today();
        pm.Description__c='testing';
        pm.RecordTypeid='0126F0000017iYR';
        pm.Status__c='Active';
        pm.People_Risks_In_The_Project__c='Low';
        pm.Project_Complexity__c='Low';
        pm.Risks_In_The_Project__c='Low';
        insert pm;
        
        Milestone1_Milestone__c mm = new Milestone1_Milestone__c();
        mm.Name='test mil';
        mm.Project__c=pm.id;
        mm.Status__c ='Active';
        insert mm;
        
        
        test.startTest();
        List<User> user_ObjTest  =  [SELECT Id,Username,LastName,Email,Alias,ProfileId,IsActive,Total_Hours_Spent__c,Total__c from User where username='systemadmin@kvpcorp.com' limit 1];
        System.debug('UserList:' +user_ObjTest);
        System.assertEquals(1,user_ObjTest.size());
        Map<Id,Milestone1_Task__c>  oldMapMilestoneTask = new Map<Id,Milestone1_Task__c>();
        Milestone1_Task__c milestone1_task_Obj = new Milestone1_Task__c();
        milestone1_task_Obj.Name = 'Name495';
        milestone1_task_Obj.Project_Module__c=pm.id; 
        milestone1_task_Obj.Project_Milestone__c=mm.id; 
        milestone1_task_Obj.Assigned_To__c = user_ObjTest[0].id; 
        milestone1_task_Obj.RecordTypeId = '0126F0000017677QAA';
        milestone1_task_Obj.Prior__c = 'P3-Low';
        milestone1_task_Obj.Start_Date1__c=system.today();
        milestone1_task_Obj.Due_Date1__c =system.today();
        
        insert milestone1_task_Obj;
        system.debug('Deepa1'+milestone1_task_Obj);
        Milestone1_Time__c ts = new Milestone1_Time__c();
        ts.Project_Task__c=milestone1_task_Obj.id;
        ts.Hours__c =20;
        system.debug('Deepa testing'+mm.Status__c+'*****'+pm.status__c+'*****'+ts.Project_Task__r.Project_Milestone__r.Project__r.Status__c);
        
        
        insert ts;
        test.stopTest();
    }
    static testMethod void test_Milestone1_TaskTrigger1(){
        User user_Obj = new User(Username = 'TestUser846620181112140956@codecoverage.com',ProfileId='00e90000000o8Kt', LastName = 'LastName649', Email = 'Email19@test.com', EmailPreferencesAutoBcc = false, EmailPreferencesAutoBccStayInTouch = false, EmailPreferencesStayInTouchReminder = false, Alias = 'Alias942', CommunityNickname = 'cNickName87878', IsActive = true, TimeZoneSidKey = 'Pacific/Kiritimati', LocaleSidKey = 'sq', ReceivesInfoEmails = false, ReceivesAdminInfoEmails = false, EmailEncodingKey = 'UTF-8', CurrencyIsoCode = 'AUD', LanguageLocaleKey = 'en_US', UserPermissionsMarketingUser = false, UserPermissionsOfflineUser = false, UserPermissionsAvantgoUser = false, UserPermissionsCallCenterAutoLogin = false, UserPermissionsMobileUser = false, UserPermissionsSFContentUser = false, UserPermissionsInteractionUser = false, UserPermissionsSupportUser = false, UserPermissionsChatterAnswersUser = false, ForecastEnabled = false, UserPreferencesActivityRemindersPopup = false, UserPreferencesEventRemindersCheckboxDefault = false, UserPreferencesTaskRemindersCheckboxDefault = false, UserPreferencesReminderSoundOff = false, UserPreferencesDisableAllFeedsEmail = false, UserPreferencesDisableFollowersEmail = false, UserPreferencesDisableProfilePostEmail = false, UserPreferencesDisableChangeCommentEmail = false, UserPreferencesDisableLaterCommentEmail = false, UserPreferencesDisProfPostCommentEmail = false, UserPreferencesContentNoEmail = false, UserPreferencesContentEmailAsAndWhen = false, UserPreferencesApexPagesDeveloperMode = false, UserPreferencesHideCSNGetChatterMobileTask = false, UserPreferencesDisableMentionsPostEmail = false, UserPreferencesDisMentionsCommentEmail = false, UserPreferencesHideCSNDesktopTask = false, UserPreferencesHideChatterOnboardingSplash = false, UserPreferencesHideSecondChatterOnboardingSplash = false, UserPreferencesDisCommentAfterLikeEmail = false, UserPreferencesDisableLikeEmail = false, UserPreferencesSortFeedByComment = false, UserPreferencesDisableMessageEmail = false, UserPreferencesDisableBookmarkEmail = false, UserPreferencesDisableSharePostEmail = false, UserPreferencesEnableAutoSubForFeeds = false, UserPreferencesDisableFileShareNotificationsForApi = false, UserPreferencesShowTitleToExternalUsers = false, UserPreferencesShowManagerToExternalUsers = false, UserPreferencesShowEmailToExternalUsers = false, UserPreferencesShowWorkPhoneToExternalUsers = false, UserPreferencesShowMobilePhoneToExternalUsers = false, UserPreferencesShowFaxToExternalUsers = false, UserPreferencesShowStreetAddressToExternalUsers = false, UserPreferencesShowCityToExternalUsers = false, UserPreferencesShowStateToExternalUsers = false, UserPreferencesShowPostalCodeToExternalUsers = false, UserPreferencesShowCountryToExternalUsers = false, UserPreferencesShowProfilePicToGuestUsers = false, UserPreferencesShowTitleToGuestUsers = false, UserPreferencesShowCityToGuestUsers = false, UserPreferencesShowStateToGuestUsers = false, UserPreferencesShowPostalCodeToGuestUsers = false, UserPreferencesShowCountryToGuestUsers = false, UserPreferencesHideS1BrowserUI = false, UserPreferencesPathAssistantCollapsed = false, UserPreferencesCacheDiagnostics = false, UserPreferencesShowEmailToGuestUsers = false, UserPreferencesShowManagerToGuestUsers = false, UserPreferencesShowWorkPhoneToGuestUsers = false, UserPreferencesShowMobilePhoneToGuestUsers = false, UserPreferencesShowFaxToGuestUsers = false, UserPreferencesShowStreetAddressToGuestUsers = false, UserPreferencesLightningExperiencePreferred = false, UserPreferencesPreviewLightning = false, UserPreferencesHideEndUserOnboardingAssistantModal = false, UserPreferencesHideLightningMigrationModal = false, UserPreferencesHideSfxWelcomeMat = false, UserPreferencesHideBiggerPhotoCallout = false, UserPreferencesGlobalNavBarWTShown = false, UserPreferencesGlobalNavGridMenuWTShown = false, UserPreferencesCreateLEXAppsWTShown = false, UserPreferencesFavoritesWTShown = false, IsPortalSelfRegistered = false, DigestFrequency = 'D', DefaultGroupNotificationFrequency = 'P', select__c = false, Opt_Email_Alert__c = false, Total_Hours_Spent__c = 162, Total__c = 163);
        
        Insert user_Obj; 
        System.debug('@@@@User@@' +user_Obj);
        Milestone1_Project__c pm = new Milestone1_Project__c();
        pm.Name='test proj module';
        pm.Kickoff__c=Date.newInstance(2016, 12, 9);
        pm.Deadline__c =Date.newInstance(2016, 12, 9);
        pm.Description__c='testing';
        pm.RecordTypeid='0126F0000017iYR';
        pm.Status__c='Active';
        pm.People_Risks_In_The_Project__c='Low';
        pm.Project_Complexity__c='Low';
        pm.Risks_In_The_Project__c='Low';
        insert pm;
        
        Milestone1_Milestone__c mm = new Milestone1_Milestone__c();
        mm.Name='test mil';
        mm.Project__c=pm.id;
        mm.Status__c ='Active';
        insert mm;
        
        
        
        List<User> user_ObjTest  =  [SELECT Id,Username,LastName,Email,Alias,ProfileId,IsActive,Total_Hours_Spent__c,Total__c from User where username='systemadmin@kvpcorp.com' limit 1];
        
        System.debug('UserList:' +user_ObjTest);
        System.assertEquals(1,user_ObjTest.size());
        
        Milestone1_Task__c milestone1_task_Obj = new Milestone1_Task__c();
        milestone1_task_Obj.Name = 'Name495';
        milestone1_task_Obj.Project_Module__c=pm.id; 
        milestone1_task_Obj.Project_Milestone__c=mm.id; 
        milestone1_task_Obj.Assigned_To__c = user_ObjTest[0].id; 
        milestone1_task_Obj.RecordTypeId = '0126F0000017677QAA';
        milestone1_task_Obj.Prior__c = 'P3-Low';
        
        insert milestone1_task_Obj;
        
        test.startTest();
        
        milestone1_task_Obj.Assigned_To__c = user_ObjTest[0].id;
        update milestone1_task_Obj;
        
        system.debug('Deepa1'+milestone1_task_Obj);
        Milestone1_Time__c ts = new Milestone1_Time__c();
        ts.Project_Task__c=milestone1_task_Obj.id;
        ts.Hours__c =20;
        system.debug('Deepa testing'+mm.Status__c+'*****'+pm.status__c+'*****'+ts.Project_Task__r.Project_Milestone__r.Project__r.Status__c);
        insert ts;
        test.stopTest();
    }
}