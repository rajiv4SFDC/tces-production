/***********************************************************
* Created By:   Ankit Kumar(KVP Business Solutions)
* Created Date: 18/July/2018
* Modified By : 
* Description : This is to send reminder message for filling the Timesheet. 
************************************************************/
public class TimeSheetReminderBatch implements Database.Batchable<sObject>,schedulable{
   public String query;
   public List<String> usrEmail = new list<String>();
   public set<Id> UsrId         = new set<Id>();
//start method     
     public Database.QueryLocator start(Database.BatchableContext BC){                         
         query = ' SELECT Id,name,IsActive,email,Opt_Email_Alert__c FROM User WHERE Opt_Email_Alert__c = False AND IsActive = True AND UserType != \'Guest\' AND UserType != \'AutomatedProcess\' AND Id NOT IN(SELECT createdById FROM Milestone1_Time__c WHERE Project_Task__c!= NULL AND CreatedDate = YESTERDAY)';         
//This query will store the list of users who have not filled the time sheet and they have opt in for email alert.
         System.debug('@@@@@@query@@@@@@@@@'+query);          
         return Database.getQueryLocator(query);
        }
//excute method       
     public void execute(Database.BatchableContext BC,List<User>usrlist){        
           System.debug('***usrlist***'+usrlist);           
           System.debug('@@@@@@usrlist@@@@@@@@@'+usrlist.Size());
           if(usrlist!=NULL){
               for(user usr:usrlist){
                  usrEmail.add(usr.email);           
               }
           }    
           System.debug('***usrEmail***'+usrEmail);
           if(usrEmail.size()>0){
              sendmail(usrEmail);
           }   
      } 
//Sending Email Method                         
      public void sendmail(List<String> str){
            System.debug('***str***'+str); 
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();        
            string [] toaddress                = str;        
            String [] ccAddresses              = new String[]{String.valueOf(Label.CcEmail)};        
            email.setSubject('Timesheet Reminder');        
            email.setHtmlBody('Hie,<br/><br/>This is to remind you that you have not filled the timesheet of previous day.<br/><br/>Regards,<br>KVP');       
            email.setToAddresses(toaddress);       
            email.setCcAddresses(ccAddresses);        
            Messaging.sendEmail(New Messaging.SingleEmailMessage[]{email}); 
      }               
//finish method  
      public void finish(Database.BatchableContext BC){   
            System.debug('***TimeSheetReminderBatch***');         
       }
//Schedule method      
      public void execute(SchedulableContext sc){
          TimeSheetReminderBatch tr = new TimeSheetReminderBatch ();
          Database.executeBatch(tr,200);
      }
}