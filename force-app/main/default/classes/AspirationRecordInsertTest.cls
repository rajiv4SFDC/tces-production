/******************************************
 * Created By  : Ajeet Singh Shekhawat
 * Created On  : 28 Aug 2018
 * Modified By : Ajeet Singh Shekhawat
 * Modified On : 28 Aug 2018
 * Description : Test-class for AspirationRecordInsert class.  
				
******************************************/ 

@isTest
public class AspirationRecordInsertTest {
    static testMethod void aspirationRecord() {
		/* for picklist mthods  */
        AspirationRecordInsert.getPicklistValuesCertificationStatus();
        AspirationRecordInsert.getPicklistValues();
        AspirationRecordInsert.getPicklistValuesIndustry();
        AspirationRecordInsert.getPicklistValuesSFProduct();
        AspirationRecordInsert.getPicklistValuesBusinessProcess();
        AspirationRecordInsert.getPicklistValuesCoreSkill();
        AspirationRecordInsert.getPicklistValuesTH();
        AspirationRecordInsert.getPicklistValuesCertificates();
        AspirationRecordInsert.getPicklistValuesApprove();
        AspirationRecordInsert.getPicklistValuesCredentials();
        AspirationRecordInsert.getPicklistValuesCertifications();
        AspirationRecordInsert.getPicklistStatusValuesTH();
        AspirationRecordInsert.getPicklistValuesCertificationStatus();
        AspirationRecordInsert.getDisplayDuration();
        /* for picklist methods  end here */
        
        /* for createBehaviourRecord methods  */
        List<Aspirations_Skill__c> aspList = new List<Aspirations_Skill__c>();
        List<Aspirations_Skill__c> aspPerList = new List<Aspirations_Skill__c>();
        List<Aspirations_Skill__c> aspProList = new List<Aspirations_Skill__c>();
        List<Aspirations_Skill__c> aspTHList = new List<Aspirations_Skill__c>();
        List<Aspirations_Skill__c> certRec = new List<Aspirations_Skill__c>();
        List<Aspirations_Skill__c> certApp = new List<Aspirations_Skill__c>();
		Aspirations_Skill__c acct, per, pro, trail, cerR, cerAp;
        String duration = 'H2';
        
        try {
        acct = new Aspirations_Skill__c();
        acct.Behaviour__c = 'Customer Handling';
        acct.Your_Knowledge_Rating__c = '6';
        acct.Support_You_Need__c = 'test';
        acct.HR_Rating__c = '4';
        acct.Manager_Rating__c = '8';
        acct.Learning_Priority__c = '4';
        acct.HR_Comment__c = 'no';
        acct.Manager_Comment__c = 'yes';
        acct.Duration__c = 'H2';
        acct.DisplayRecord__c = false;
        aspList.add(acct);
        
        insert aspList;        
        String aspirationSkillList = JSON.serialize(aspList);
        
        AspirationRecordInsert.createBehaviourRecord(aspirationSkillList, duration);
        AspirationRecordInsert.createIndustryRecord(aspirationSkillList, duration);
        AspirationRecordInsert.createSFProductRecord(aspirationSkillList, duration);
        AspirationRecordInsert.createBusinessProcessRecord(aspirationSkillList, duration);
        AspirationRecordInsert.createCoreSkillRecord(aspirationSkillList, duration);
        /* for createBehaviourRecord methods end here */
        
		per = new Aspirations_Skill__c();
        per.Describe_yourself_in_few_words__c = 't';
        per.Short_Term_Personal_Goals__c = 'y';
        per.Long_Term_Personal_Goals__c = 'h';
        per.What_activities_give_you_joy_excitement__c = 'g';
        per.Support_require_to_realize_your_goals__c = 'h';
        per.Changes_you_want_to_yourself__c = 'l';
        per.Duration__c = duration; 
        aspPerList.add(per); 
        insert aspPerList;        
        String asr = JSON.serialize(aspPerList);
        AspirationRecordInsert.createAspPersonalRecord(asr, duration);
        
        pro = new Aspirations_Skill__c();
        pro.Type_Of_Project_You_Want_To_Work__c = 'g';
        pro.What_Is_Exciting_In_Your_Project__c = 'g';
        pro.What_activities_give_you_joy_excitement__c = 'g';
        pro.Top_3_Things_You_Want_To_Learn__c = 'g';
        pro.Support_require_to_realize_your_goals__c = 'k';
        pro.Where_Do_You_See_Yourself_in_3_Years__c = 'k';
        pro.Duration__c = duration; 
        aspProList.add(pro);
        insert aspProList;        
        String asrPro = JSON.serialize(aspProList);
        AspirationRecordInsert.createAspProfessionalRecord(asrPro, duration);
        
        } 
        catch (Exception ex){
            throw new AuraHandledException('Something went wrong: '+ ex.getMessage());
        } 
        
        trail = new Aspirations_Skill__c();
        trail.Plan_For_the_Month__c = 'January';
        trail.of_trails_I_plan_to_achieve_this_month__c = 5;
        trail.of_points_I_plan_to_achieve_this_month__c = 5;
        trail.of_badges_I_plan_to_achieve_this_month__c = 5;
        trail.Name_of_the_badges_I_plan_to_achieve__c = 'h';
        trail.Status__c = 'Plan';
        trail.of_trail_I_completed_in_this_month__c = 5;
        trail.of_points_I_completed_in_this_month__c = 5;
        trail.Name_the_badges_completed_in_this_month__c = 'h';
        trail.of_badges_I_completed_in_this_month__c = 8;
        trail.My_trail_head_page__c = 'n';
        trail.Reason_for_the_gap__c = 'n';
        trail.Duration__c = duration; 
        aspTHList.add(trail);
        insert aspTHList;        
        String asrTH = JSON.serialize(aspTHList);
        AspirationRecordInsert.createTHRecord(asrTH, duration);
        
        cerR = new Aspirations_Skill__c();
        cerR.Certifications__c = 'Salesforce Administrator 201';
       // cerR.Other_Certifications__c = 'g';
        cerR.Is_It_Linked_to_KVP_Portal__c = true;                    
        cerR.Certification_valid_up_to__c = System.today();
        cerR.Webassesor_link__c = 'j';
        cerR.Approval_Status__c = 'Validated by HR';
        cerR.Duration__c = duration; 
        certRec.add(cerR);
        insert certRec;        
        String asrCer = JSON.serialize(certRec);
        AspirationRecordInsert.createCertificateRecord(asrCer, duration); 
        
        cerAp = new Aspirations_Skill__c();
        cerAp.Certifications__c = 'Salesforce Platform Developer 1';
        cerAp.Credentials__c = 'App Builders';
     //   cerAp.Other_Certifications__c = 'fdf';                    
        cerAp.Date_of_Certification__c = System.today();
        cerAp.Certification_Status__c = 'Conditionally Approved';
        cerAp.Credential_Overview_Link__c = 'Validated by HR';
        cerAp.Support_You_Need__c = 'j';
        cerAp.Steps_You_Plan_to_Take_to_Improve__c = 'j';
        cerAp.Your_Knowledge_Rating__c = '5';
        cerAp.Duration__c = duration; 
        certApp.add(cerAp);
        insert certApp;        
        String asrApp = JSON.serialize(certApp);
        AspirationRecordInsert.createCerApplyRecord(asrApp, duration); 
    }
}